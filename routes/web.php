<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin Routes
Route::patch('status/{id}/{status}', 'Admin\SubmissionController@status')->name('admin.submissions.status');
Route::get('status/{id}/{status}', 'Admin\SubmissionController@status')->name('admin.submissions.status.get');


Route::prefix(config('app.admin_path'))->name('admin.')->namespace('Admin')->group(function () {
	Route::get('/', function () {
		return redirect()->route('admin.home');
	});

	Route::get('login', 'AuthController@index')->name('login');
	Route::post('login', 'AuthController@login')->name('login');
	Route::post('logout', 'AuthController@logout')->name('logout');

	Route::prefix('form-surat')->group(function () {
		Route::get('/', 'FormSuratController@index')->name('formsurat');
		Route::post('store/{letter}', 'FormSuratController@store')->name('formsurat.store');
	});

	Route::prefix('pencatatan')->group(function () {
		Route::get('/', 'PencatatanSuratController@index')->name('pencatatan');
		Route::post('store', 'PencatatanSuratController@store')->name('pencatatan.store');
	});
	Route::prefix('laporan')->group(function () {
		Route::get('{letter}', 'LaporanController@index')->name('laporan');
		Route::post('cetak', 'LaporanController@cetak')->name('laporan.cetak');
		Route::post('cetaks', 'LaporanController@cetakPemerintahan')->name('laporan.cetak.kpemerintahan');
		Route::post('cetak/masuk', 'LaporanController@cetakMasuk')->name('laporan.cetak.masuk');

		Route::get('masuk/{jenis}', 'LaporanController@indexMasuk')->name('laporan.masuk');
		Route::get('q/{letter}', 'LaporanController@indexQ')->name('laporan.q');
	});

	Route::prefix('archieve')->group(function () {
		Route::prefix('in')->group(function () {
			Route::get('{cat}', 'ArsipController@index')->name('arsip');
			Route::get('detail/{id}', 'ArsipController@detail')->name('arsip.detail');
		});
		Route::prefix('out')->group(function () {
			Route::get('{letter}', 'ArsipController@indexOut')->name('arsip.out');
			Route::get('q/{keterangan}', 'ArsipController@indexOutQ')->name('arsip.out.q');
			Route::get('detail/{id}', 'ArsipController@detailOut')->name('arsip.out.detail');
		});
	});

	Route::middleware('auth:admin')->group(function () {
		Route::get('home', 'HomeController@index')->name('home');

		Route::prefix('submissions')->name('submissions.')->group(function () {
			Route::get('pending', 'SubmissionController@pending')->name('pending');
			Route::get('approved', 'SubmissionController@approved')->name('approved');
			Route::get('selesai', 'SubmissionController@selesai')->name('selesai');
			Route::post('approved/export', 'SubmissionController@exportApproved')->name('approved.export');
			Route::get('rejected', 'SubmissionController@rejected')->name('rejected');
			Route::get('show/{id}', 'SubmissionController@show')->name('show');

			Route::patch('update/{id}', 'SubmissionController@update')->name('update');
			Route::post('print/{id}', 'SubmissionController@print')->name('print');
		});

		Route::resource('data', 'AdminController');
		Route::resource('users', 'UserController');
		Route::resource('letters', 'LetterController');
		Route::resource('signatories', 'SignatoryController');
		Route::view('helpers', 'admin.helper.index')->name('helpers');

		Route::prefix('user')->group(function () {
			Route::get('approval/{status}/{id}', 'UserController@approval')->name('user.approval');
		});

		Route::prefix('import')->name('import.')->group(function () {
			Route::post('data', 'ImportController@admin')->name('data');
			Route::post('users', 'ImportController@user')->name('users');
		});

		Route::prefix('account')->group(function () {
			Route::get('/', 'AccountController@index')->name('account');
			Route::patch('/', 'AccountController@update')->name('account');
			Route::name('account.')->group(function () {
				Route::get('password', 'AccountController@password')->name('password');
				Route::patch('password', 'AccountController@patchPassword')->name('password');
				Route::get('logs', 'AccountController@logs')->name('logs');
			});
		});

		Route::prefix('settings')->name('settings')->group(function () {
			Route::get('/', 'SettingController@index');
			Route::patch('/', 'SettingController@update');
		});

		Route::prefix('datatables')->name('datatables.')->group(function () {
			Route::post('users', 'DataController@users')->name('users');
			Route::post('submissions/pending', 'DataController@submissionsPending')->name('submissions.pending');
			Route::post('submissions/approved', 'DataController@submissionsApproved')->name('submissions.approved');
			Route::post('submissions/rejected', 'DataController@submissionsRejected')->name('submissions.rejected');
			Route::post('submissions/selesai', 'DataController@submissionSelesai')->name('submissions.selesai');
		});
	});
});

// User Routes
Route::get('/', 'HomeController@index2');

Route::get('login', 'AuthController@index')->name('login');
Route::get('registrasi', 'AuthController@registrasi')->name('registrasi');
Route::post('registrasi', 'AuthController@registrasiStore')->name('registrasi.store');
Route::post('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@login')->name('login');
Route::post('lupa-password', 'AuthController@lupaPassword')->name('lupa');
Route::post('lupa-password-proses/{id}', 'AuthController@lupaPasswordProses')->name('lupa.proses');
Route::get('reset-password/{token}', 'AuthController@resetPassword')->name('reset.password');
Route::post('logout', 'AuthController@logout')->name('logout');

Route::middleware('auth')->group(function () {
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('submissions', 'SubmissionController@index')->name('submissions.index');
	Route::get('submissions/add', 'SubmissionController@add')->name('submissions.add');
	Route::post('submissions/store/{letter}', 'SubmissionController@store')->name('submissions.store');

	Route::prefix('account')->group(function () {
		Route::get('/', 'AccountController@index')->name('account');
		Route::get('/edit', 'AccountController@indexEdit')->name('account.edit');
		Route::post('/update', 'AccountController@update')->name('account.update');
		Route::name('account.')->group(function () {
			Route::post('whatsapp', 'AccountController@whatsapp')->name('whatsapp');
			Route::get('password', 'AccountController@password')->name('password');
			Route::patch('password', 'AccountController@patchPassword')->name('password');
			Route::get('logs', 'AccountController@logs')->name('logs');
		});
	});

	Route::prefix('laporan')->group(function () {
		Route::get('{letter}', 'LaporanController@index')->name('laporan');
	});

	Route::prefix('archieve')->group(function () {
		Route::prefix('in')->group(function () {
			Route::get('{cat}', 'ArsipController@index')->name('arsip');
			Route::get('detail/{id}', 'ArsipController@detail')->name('arsip.detail');
		});
		Route::prefix('out')->group(function () {
			Route::get('{letter}', 'ArsipController@indexOut')->name('arsip.out');
			Route::get('q/{keterangan}', 'ArsipController@indexOutQ')->name('arsip.out.q');
			Route::get('detail/{id}', 'ArsipController@detailOut')->name('arsip.out.detail');
		});
	});
	Route::prefix('laporan')->group(function () {
		Route::get('{letter}', 'LaporanController@index')->name('laporan');
		Route::post('cetak', 'LaporanController@cetak')->name('laporan.cetak');
		Route::get('masuk/{jenis}', 'LaporanController@indexMasuk')->name('laporan.masuk');
		Route::get('q/{letter}', 'LaporanController@indexQ')->name('laporan.q');
	});
	Route::prefix('submissions')->name('submissions.')->group(function () {
		Route::get('pending', 'SubmissionController@pending')->name('pending');
		Route::get('approved', 'SubmissionController@approved')->name('approved');
		Route::get('selesai', 'SubmissionController@selesai')->name('selesai');
		Route::post('print/{id}', 'Admin\SubmissionController@print')->name('print');
		Route::get('show/{id}', 'SubmissionController@show')->name('show');
	});
});
