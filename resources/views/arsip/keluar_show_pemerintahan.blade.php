

@extends('layouts.master')

@section('title', 'Detail Pengajuan Surat')

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('components.breadcumb')
                @slot('title')
                    Detail Arsip Surat
                @endslot
                @slot('li_1')
                    Kepala Desa
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class='table table-bordered'>
                        <tbody>
                            <tr class='active'>
                                <td colspan="2"><strong><i class='fa fa-bars'></i> Detail Arsip Surat Bernomor
                                        {{ $arsip->letter_number }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>No Surat</strong></td>
                                <td>{{ $arsip->letter_number }}</td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Surat</strong></td>
                                <td>Surat Keluar</td>
                            </tr>
                            <tr>
                                <td><strong>Kategori Surat</strong></td>
                                <td>{{ ucfirst($arsip->letter_category) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Diterima Keluarkan Oleh</strong></td>
                                <td>{{ $arsip->out_by }}</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Surat Dikeluarkan</strong></td>
                                <td>{{ date('d M Y', strtotime($arsip->letter_date_out)) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Sifat Surat</strong></td>
                                <td>{{ ucfirst($arsip->letter_trait) }}</td>
                            </tr>
                            {{-- <tr>
                                <td><strong>Perihal</strong></td>
                                <td>{{ ucfirst($arsip->regarding) }}</td>
                            </tr> --}}
                            <tr>
                                <td><strong>Tujuan Surat</strong></td>
                                <td>{{ ucfirst($arsip->to) }}</td>
                            </tr>
                            <tr>
                                <td><strong>Lampiran</strong></td>
                                <td><a href="{{ asset($arsip->file) }}" target="_blank"><u>Lihat Lampiran</u></a></td>
                            </tr>

                        </tbody>
                    </table>
                    <a href="{{ url()->previous() }}" class="float-right btn btn-warning">Kembali</a>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/tinymce/tinymce.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>

@endsection
