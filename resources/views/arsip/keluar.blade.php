@extends('layouts.master')

@section('title', 'Pengajuan Surat: Menunggu Persetujuan')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('components.breadcumb')
                @slot('title')
                    Arsip Surat Keluar : {{ $letter->name }}
                    {{ $letter->id == 4 ? ($k == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)') : '' }}
                @endslot
                @slot('li_1')
                    Kepala Desa
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- <h4 class="card-title">Pengajuan Surat: Menunggu Persetujuan</h4> --}}
                    @include('components.message')
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Surat</th>
                                <th>Tanggal Dikeluarkan</th>
                                <th>Penerima</th>
                                <th>Dikeluarkan Oleh</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($arsip as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->submission->number }}</td>
                                    <td>{{ date('d M Y', strtotime($item->letter_date_out)) }}</td>
                                    <td>{{ $item->to }}</td>
                                    <td>{{ $item->out_by }}</td>
                                    {{-- <td>{{ ucfirst($item->letter_trait) }}</td> --}}
                                    <td>
                                        {{-- <form method="POST"
                                            action="{{ route('submissions.print', [$item->submission->id]) }}"
                                            class="d-inline" target="_blank">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-warning waves-effect waves-light"><i
                                                    class="mdi mdi-printer-check"></i> Cetak</button>
                                        </form> --}}
                                        <a href="{{ route('arsip.out.detail', $item->id) }}"
                                            class="btn btn-sm btn-primary waves-effect waves-light"><i
                                                class="mdi mdi-eye-circle"></i> Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <!-- Button trigger modal -->

    <!-- Modal -->


@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>

    <script type="text/javascript">
        $(document).on('submit', '.form-patch', function(e) {
            var form = this;
            e.preventDefault();
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes!"
            }).then(function(result) {
                if (result.value) {
                    return form.submit();
                }
            });
        });
        // $(document).on('submit', '.form-patch-reject', function(e) {
        //     $('#modalReject').modal('show');
        // });
    </script>

@endsection
