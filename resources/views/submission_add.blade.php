@extends('layouts.master')

@section('title', 'Home')

@section('content')
    <!-- start page title -->

    <!-- end page title -->
    {{-- @if (!auth()->user()->phone_number)
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Tambahkan Nomor Whatsapp</h4>
                <p class="card-title-desc">Dapatkan pemberitahun mengenai pengajuan surat melalui Whatsapp. ex: 6281234567890 (jika format salah, pesan tidak akan terkirim)</p>

                <form class="custom-validation" method="POST" action="{{ route('account.whatsapp') }}"
                    enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="phone_number" class="col-sm-2 col-form-label">Nomor Whatsapp</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" name="phone_number" id="phone_number"
                                value="{{ old('phone_number', auth()->user()->phone_number ?? '628') }}" data-parsley-length="[5,255]" required>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
@endif --}}

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Pengajuan</h4>
                    <p class="card-title-desc">Silahkan Klik Form Surat Yang Tersedia & Isi Dengan Benar</p>
                    @include('components.message')
                    <div id="accordion">
                        @foreach ($letters as $letter)
                            <div class="card mb-1">
                                <div class="card-header p-3" id="heading{{ $letter->id }}">
                                    <h6 class="m-0 font-14">
                                        <a href="#collapse{{ $letter->id }}" class="text-dark" data-toggle="collapse"
                                            aria-expanded="true"
                                            aria-controls="collapse{{ $letter->id }}">{{ $letter->name }}</a>
                                    </h6>
                                </div>

                                <div id="collapse{{ $letter->id }}" class="collapse"
                                    aria-labelledby="heading{{ $letter->id }}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            {!! $letter->information !!}
                                        </div>
                                        <form class="custom-validation" method="POST"
                                            action="{{ route('submissions.store', $letter->id) }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="user_id" class="col-sm-2 col-form-label">Kepentingan
                                                    Surat</label>
                                                <div class="col-sm-10">
                                                    <select name="kepentingan_surat"
                                                        onchange="generateUser('{{ $letter->id }}')"
                                                        id="kepentingan_surat_{{ $letter->id }}" class="form-control"
                                                        required style="width:100%">
                                                        <option value="">- Pilih -</option>
                                                        <option value="Diri Sendiri">Untuk Diri Sendiri</option>
                                                        <option value="Orang Lain">Untuk Orang Lain</option>
                                                    </select>
                                                </div>
                                            </div>
                                            @if ($letter->id == 4)
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Keterangan
                                                        Pengajuan</label>
                                                    <div class="col-sm-10">
                                                        <select name="keterangan_pengajuan"
                                                            onchange="getSyarat(this,'{{ $letter->id }}')" required
                                                            class="form-control">
                                                            <option value="">- Pilih -</option>
                                                            <option value="1">Belum Pernah Melakukan Perekaman E-KTP
                                                            </option>
                                                            <option value="2">Belum Memiliki Akta Kelahiran</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row b-usia_syarat" style="display: none">
                                                    <label for="" class="col-sm-2 col-form-label">Usia</label>
                                                    <div class="col-sm-10">
                                                        <select name="usia_syarat" id="usia_syarat"
                                                            onchange="getSyaratFile(this,'{{ $letter->id }}')"
                                                            class="form-control">
                                                            <option value="">- Pilih -</option>
                                                            <option value="1">Usia 18 Tahun
                                                            </option>
                                                            <option value="0">Dibawah 18 Tahun</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            @foreach (json_decode($letter->data) as $data)
                                                <div
                                                    class="form-group row bparent_{{ $letter->id }}_{{ !empty($data->is_special) ? ($data->is_special == 'ya' ? $data->parents : '') : '' }}">
                                                    <label for="{{ $data->input_name }}"
                                                        class="col-sm-2 col-form-label">{{ $data->input_label }}</label>
                                                    <div class="col-sm-10">

                                                        @switch($data->input_type)
                                                            @case('number')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="number" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    value="{{ old($data->input_name) }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                            @break

                                                            @case('text')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="text" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    value="{{ old($data->input_name) }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                            @break

                                                            @case('file')
                                                                <input
                                                                    class="form-control input_{{ $letter->id }}_{{ !empty($data->is_special) ? ($data->is_special == 'ya' ? $data->parents : '') : '' }}"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="file" accept="application/pdf, image/jpeg, image/png"
                                                                    name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    value="{{ old($data->input_name) }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                <small>Format yang didukung : jpg,png,pdf</small>
                                                            @break

                                                            @case('date')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="date" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    value="{{ old($data->input_name) }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                {{-- <small>Format yang didukung : jpg,png,pdf</small> --}}
                                                            @break

                                                            @default
                                                                <textarea name="{{ $data->input_name }}" id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}" class="form-control"
                                                                    rows="3">{{ old($data->input_name) }}</textarea>
                                                        @endswitch

                                                    </div>
                                                </div>
                                            @endforeach
                                            {{-- @foreach (json_decode($letter->data) as $data)
                                                <div class="form-group row">
                                                    <label for="{{ $data->input_name }}"
                                                        class="col-sm-2 col-form-label">{{ $data->input_label }}</label>
                                                    <div class="col-sm-10">
                                                        @switch($data->input_type)
                                                            @case('number')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="number" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    {{ !empty($data->input_required) ? $data->input_required == 'ya' ? 'required' : '' : '' }}
                                                                    value="{{ old($data->input_name) }}">
                                                            @break

                                                            @case('text')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="text" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    {{ !empty($data->input_required) ? $data->input_required == 'ya' ? 'required' : '' : '' }}
                                                                    value="{{ old($data->input_name) }}">
                                                            @break

                                                            @case('file')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="file" accept="application/pdf, image/jpeg, image/png"
                                                                    name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    {{ !empty($data->input_required) ? $data->input_required == 'ya' ? 'required' : '' : '' }}
                                                                    value="{{ old($data->input_name) }}">
                                                                <small>Format yang didukung : jpg,png,pdf</small>
                                                            @break

                                                            @case('date')
                                                                <input class="form-control"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="date" name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    {{ !empty($data->input_required) ? $data->input_required == 'ya' ? 'required' : '' : '' }}
                                                                    value="{{ old($data->input_name) }}">
                                                            @break

                                                            @default
                                                                <textarea name="{{ $data->input_name }}"
                                                                    {{ !empty($data->input_required) ? $data->input_required == 'ya' ? 'required' : '' : '' }}
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}" class="form-control"
                                                                    rows="3">{{ old($data->input_name) }}</textarea>
                                                        @endswitch

                                                    </div>
                                                </div>
                                            @endforeach
                                            @if ($letter->id == 4)
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Keterangan
                                                        Pengajuan</label>
                                                    <div class="col-sm-10">
                                                        <select name="keterangan_pengajuan" class="form-control">
                                                            <option value="1">Belum Pernah Melakukan Perekaman E-KTP
                                                            </option>
                                                            <option value="2">Belum Memiliki Akta Kelahiran</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif --}}

                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit"
                                                        class="btn btn-primary waves-effect waves-light mr-1">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function getSyarat(onbj, letter_id) {
            let keterangan_pengajuan = $(onbj).find(':selected').val();
            console.log(keterangan_pengajuan);
            if (keterangan_pengajuan == 2) {
                $('.b-usia_syarat').show();
                $('#usia_syarat').prop('required', true);

            } else {
                $('.b-usia_syarat').hide();
                $('#usia_syarat').prop('required', false);
                $('.input_' + letter_id + '_diatas').prop('required', false);
                $('.input_' + letter_id + '_dibawah').prop('required', false);
                $('.bparent_' + letter_id + '_diatas').hide();
                $('.bparent_' + letter_id + '_dibawah').hide();
            }
        }

        function getSyaratFile(obj, letter_id) {
            console.log('.bparent_' + letter_id + '_diatas')
            let syarat = $(obj).find(':selected').val();
            if (syarat == 1) {
                // console.log('tes')
                $('.input_' + letter_id + '_diatas').prop('required', true);
                $('.input_' + letter_id + '_dibawah').prop('required', false);
                $('.bparent_' + letter_id + '_diatas').show();
                $('.bparent_' + letter_id + '_dibawah').hide();
            } else {
                $('.input_' + letter_id + '_diatas').prop('required', false);
                $('.input_' + letter_id + '_dibawah').prop('required', true);
                $('.bparent_' + letter_id + '_diatas').hide();
                $('.bparent_' + letter_id + '_dibawah').show();
            }
        }

        function generateUser(letter_id) {
            let kepentingan = $('#kepentingan_surat_' + letter_id).find(':selected').val();
            console.log(kepentingan);


            if (kepentingan == 'Diri Sendiri') {

                let nama_lengkap = '{{ auth()->user()->name }}'
                let tempat_lahir = '{{ auth()->user()->birth_place }}'
                let tanggal_lahir = '{{ auth()->user()->birth_date }}'
                let jenis_kelamin = '{{ auth()->user()->gender }}'
                let status_perkawinan = '{{ auth()->user()->marital_status }}'
                let agama = '{{ auth()->user()->religion }}'
                let pekerjaan = '{{ auth()->user()->profession }}'
                let alamat = '{{ auth()->user()->address }}'
                let nik = '{{ auth()->user()->sin }}'
                // console.log(nama_lengkap,tempat_lahir,tanggal_lahir,jenis_kelamin,status_perkawinan,agama,pekerjaan,alamat);
                $('#nama_lengkap_' + letter_id).val(nama_lengkap);
                $('#tempat_lahir_' + letter_id).val(tempat_lahir);
                $('#tanggal_lahir_' + letter_id).val(tanggal_lahir.replace(" 00:00:00", ''));
                $('#jenis_kelamin_' + letter_id).val(jenis_kelamin);
                $('#status_perkawinan_' + letter_id).val(status_perkawinan);
                $('#agama_' + letter_id).val(agama);
                $('#pekerjaan_' + letter_id).val(pekerjaan);
                $('#alamat_' + letter_id).val(alamat);
                $('#alamat_' + letter_id).text(alamat);
                $('#nik_' + letter_id).val(nik);
                $('#kewarganegaraan_' + letter_id).val('WNI');
            } else {
                $('#nama_lengkap_' + letter_id).val('');
                $('#tempat_lahir_' + letter_id).val('');
                $('#tanggal_lahir_' + letter_id).val('');
                $('#jenis_kelamin_' + letter_id).val('');
                $('#status_perkawinan_' + letter_id).val('');
                $('#agama_' + letter_id).val('');
                $('#pekerjaan_' + letter_id).val('');
                $('#alamat_' + letter_id).val('');
                $('#nik_' + letter_id).val('');
                $('#alamat_' + letter_id).text('');
                ('#kewarganegaraan_' + letter_id).val('');
            }
        }
    </script>
@endsection
