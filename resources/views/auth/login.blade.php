@extends('admin.layouts.app')

@section('title', 'Login')

@section('body')

    <body>
    @endsection

    @section('content')

        <div class="account-pages my-5 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-primary">
                                <div class="text-primary text-center p-4">
                                    <h5 class="text-white font-size-20">Selamat Datang di Sistem Informasi Pelayanan Administrasi Surat Menyurat Desa Parit Baru</h5>
                                    <p class="text-white-50">Silahkan login terlebih dahulu.</p>
                                    <a href="#" class="logo logo-admin">
                                        <img src="{{ asset('assets/logo1.png') }}" style="width:100%;object-fit: cover"
                                            alt="logo">
                                    </a>
                                </div>
                            </div>

                            <div class="card-body p-4">
                                <div class="p-3">
                                    @if (session('gagal'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>{{ session('gagal') }}</strong>
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>{{ session('success') }}</strong>
                                        </div>
                                    @endif
                                    <form class="form-horizontal mt-4" method="POST" action="{{ route('login') }}">
                                        @csrf

                                        <div class="form-group">
                                            <label for="sin">NIK</label>
                                            <input name="sin" type="text"
                                                class="form-control @error('sin') is-invalid @enderror"
                                                value="{{ old('sin') }}" id="sin" placeholder="Masukkan NIK KTP"
                                                autocomplete="sin" autofocus>
                                            @error('sin')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password"
                                                class="form-control  @error('password') is-invalid @enderror" id="password"
                                                placeholder="Masukkan Password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-4 text-right" style="margin-right:-13px ">
                                                <a href="{{ url('/') }}"
                                                    class="btn btn-secondary btn-block w-md waves-effect waves-light">Home</a>

                                            </div>
                                            <div class="col-sm-4 text-left">
                                                <button class="btn btn-primary btn-block w-md waves-effect waves-light"
                                                    type="submit">Masuk</button>

                                            </div>
                                            <div class="col-sm-12 mt-3 text-center">
                                                Belum punya akun ? <a href="{{ route('registrasi') }}"><u>Registrasi di
                                                        sini</u></a> <br>
                                                Lupa Password ? <a href="javascript:void(0)" data-toggle="modal"
                                                    data-target="#modalLupa"><u>Klik di sini</u></a>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>

                        <div class="mt-5 text-center">
                            <p class="mb-0">©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> Pemerintah Desa Parit Baru
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="modalLupa" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Lupa Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('lupa') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" required class="form-control" name="email">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection
