@extends('admin.layouts.app')

@section('title', 'Login')

@section('body')

    <body>
    @endsection

    @section('content')

        <div class="account-pages my-5 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-8">
                        <div class="card overflow-hidden">
                            <div class="bg-primary">
                                <div class="text-primary text-center p-4">
                                    <h5 class="text-white font-size-20">Selamat Datang di Sistem Informasi Pelayanan <br>
                                        Administrasi Surat Menyurat Desa parit Baru</h5>

                                    <p class="text-white-50">Silahkan melakukan registrasi di bawah ini.</p>
                                    <a href="#" class="logo logo-admin">
                                        <img src="{{ asset('assets/logo1.png') }}" style="width:100%;object-fit: cover"
                                            alt="logo">
                                    </a>
                                </div>
                            </div>

                            <div class="card-body p-4 mt-5">
                                <div class="p-3">
                                    {{-- <div class="alert alert-warning" role="alert">
                                        <strong>Registrasi ini hanya untuk warga Desa Parit Baru, akan dilakukan verifikasi
                                            setelah pendaftaran dilakukan</strong>
                                    </div> --}}
                                    @if (session('success'))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{ session('success') }}</strong>
                                        </div>
                                    @endif
                                    @if (session('error'))
                                        @php
                                            $error_email = null;
                                            $error_nik = null;
                                            
                                            if (session('error')['type'] == 'email') {
                                                $error_email = session('error')['message'];
                                            } elseif (session('error')['type'] == 'nik') {
                                                $error_nik = session('error')['message'];
                                            }
                                            
                                            // dump($error);
                                            
                                        @endphp
                                        {{-- <div class="alert alert-danger" role="alert">
                                            <strong>{{ session('error')['message'] }}</strong>
                                        </div> --}}
                                    @endif
                                    <form class="custom-validation" method="POST" action="{{ route('registrasi.store') }}"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="photo" class="col-sm-3 col-form-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" required type="file" name="photo"
                                                    id="photo">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="sin" class="col-sm-3 col-form-label">NIK *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control {{ !empty($error_nik) ? 'is-invalid' : '' }}"
                                                    maxlength="16" minlength="16" type="text" name="sin"
                                                    id="sin"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['sin'] : '' }}"
                                                    data-parsley-type="number" required>
                                                @if (!empty($error_nik))
                                                    <div id="sin" class="invalid-feedback">
                                                        {{ $error_nik }}
                                                    </div>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Nama *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="name" id="name"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['name'] : '' }}"
                                                    data-parsley-length="[5,255]" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Email *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control {{ !empty($error_email) ? 'is-invalid' : '' }}"
                                                    type="email" name="email" id="email"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['email'] : '' }}"
                                                    required>
                                                @if (!empty($error_email))
                                                    <div id="sin" class="invalid-feedback">
                                                        {{ $error_email }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="birth_place" class="col-sm-3 col-form-label">Tempat Lahir *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="birth_place"
                                                    id="birth_place"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['birth_place'] : '' }}"
                                                    data-parsley-length="[5,255]" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="birth_date" class="col-sm-3 col-form-label">Tanggal Lahir *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="birth_date" id="birth_date"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['birth_date'] : '' }}"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="gender" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="gender" id="gender" required>
                                                    <option selected disabled>Select *</option>
                                                    @if (!empty(session('error')))
                                                        <option
                                                            {{ session('error')['req']['gender'] == 'Laki - Laki' ? 'selected' : '' }}
                                                            value="Laki - Laki">Laki
                                                            - Laki</option>
                                                        <option
                                                            {{ session('error')['req']['gender'] == 'Perempuan' ? 'selected' : '' }}
                                                            value="Perempuan">Perempuan
                                                        </option>
                                                    @else
                                                        <option {{ old('gender') == 'Laki - Laki' ? 'selected' : '' }}
                                                            value="Laki - Laki">Laki
                                                            - Laki</option>
                                                        <option {{ old('gender') == 'Perempuan' ? 'selected' : '' }}
                                                            value="Perempuan">Perempuan
                                                        </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <textarea id="address" name="address" class="form-control" maxlength="225" rows="3">{{ !empty(session('error')) ? session('error')['req']['address'] : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="religion" class="col-sm-3 col-form-label">Agama</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="religion" id="religion" required>
                                                    <option selected disabled>Select *</option>
                                                    @if (!empty(session('error')))
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Islam' ? 'selected' : '' }}
                                                            value="Islam">Islam</option>
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Protestan' ? 'selected' : '' }}
                                                            value="Protestan">
                                                            Protestan</option>
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Katolik' ? 'selected' : '' }}
                                                            value="Katolik">Katolik
                                                        </option>
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Hindu' ? 'selected' : '' }}
                                                            value="Hindu">Hindu</option>
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Buddha' ? 'selected' : '' }}
                                                            value="Buddha">Buddha
                                                        </option>
                                                        <option
                                                            {{ session('error')['req']['religion'] == 'Konghucu' ? 'selected' : '' }}
                                                            value="Konghucu">Konghucu
                                                        </option>
                                                    @else
                                                        <option {{ old('religion') == 'Islam' ? 'selected' : '' }}
                                                            value="Islam">Islam</option>
                                                        <option {{ old('religion') == 'Protestan' ? 'selected' : '' }}
                                                            value="Protestan">
                                                            Protestan</option>
                                                        <option {{ old('religion') == 'Katolik' ? 'selected' : '' }}
                                                            value="Katolik">Katolik
                                                        </option>
                                                        <option {{ old('religion') == 'Hindu' ? 'selected' : '' }}
                                                            value="Hindu">Hindu</option>
                                                        <option {{ old('religion') == 'Buddha' ? 'selected' : '' }}
                                                            value="Buddha">Buddha
                                                        </option>
                                                        <option {{ old('religion') == 'Konghucu' ? 'selected' : '' }}
                                                            value="Konghucu">Konghucu
                                                        </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="marital_status" class="col-sm-3 col-form-label">Status
                                                Perkawinan</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="marital_status" id="marital_status"
                                                    required>
                                                    <option selected disabled>Select *</option>
                                                    @if (!empty(session('error')))
                                                        <option
                                                            {{ session('error')['req']['marital_status'] == 'Belum Kawin' ? 'selected' : '' }}
                                                            value="Belum Kawin">Belum Kawin</option>
                                                        <option
                                                            {{ session('error')['req']['marital_status'] == 'Kawin' ? 'selected' : '' }}
                                                            value="Kawin">Kawin
                                                        </option>
                                                    @else
                                                        <option
                                                            {{ old('marital_status') == 'Belum Kawin' ? 'selected' : '' }}
                                                            value="Belum Kawin">Belum Kawin</option>
                                                        <option {{ old('marital_status') == 'Kawin' ? 'selected' : '' }}
                                                            value="Kawin">Kawin
                                                        </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="profession" class="col-sm-3 col-form-label">Pekerjaan *</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="profession"
                                                    id="profession"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['profession'] : '' }}"
                                                    required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone_number" class="col-sm-3 col-form-label">Phone Number</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="phone_number"
                                                    id="phone_number"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['phone_number'] : '' }}"
                                                    required>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="phone_number" class="ol-sm-3 col-form-label">Phone Number</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="phone_number"
                                                    id="phone_number"
                                                    value="{{ !empty(session('error')) ? session('error')['req']['phone_number'] : '' }}" required>
                                            </div>
                                        </div> --}}
                                        <div class="form-group row">
                                            <label for="password" class="col-sm-3 col-form-label">Password</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" required type="password" name="password"
                                                    id="password" data-parsley-length="[6,255]">
                                            </div>
                                        </div>
                                        <div class="form-group mb-0">
                                            <div>
                                                <button type="submit"
                                                    class="btn btn-primary waves-effect waves-light mr-1">
                                                    Submit
                                                </button>
                                                <a class="btn btn-secondary waves-effect waves-light"
                                                    href="{{ route('login') }}" role="button">Kembali Ke Halaman
                                                    Login</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                        <div class="mt-5 text-center">
                            <p class="mb-0">©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script> Pemerintah Desa Parit Baru
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endsection
