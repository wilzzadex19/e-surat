<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ route('admin.home') }}" class="waves-effect">
                        <i class="ti-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.formsurat') }}" class="waves-effect">
                        <i class="ti-pencil-alt"></i>
                        <span>Form Surat</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ti-email"></i>
                        <span>Layanan Surat Masuk</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('admin.submissions.pending') }}">
                                <span
                                    class="badge badge-pill badge-primary float-right">{{ pendingSubmissions() }}</span>
                                Menunggu Persetujuan </a></li>
                        <li><a href="{{ route('admin.submissions.approved') }}">Disetujui</a></li>
                        <li><a href="{{ route('admin.submissions.rejected') }}">Ditolak</a></li>
                        <li><a href="{{ route('admin.submissions.selesai') }}">Selesai</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('admin.pencatatan') }}" class="waves-effect">
                        <i class="ti-pencil-alt"></i>
                        <span>Pencatatan Surat</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ti-folder"></i>
                        <span>Arsip Surat</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li>
                            <a href="#" class="has-arrow waves-effect">
                                <i class="ti-download"></i>
                                <span>Surat Masuk Pemerintahan</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">

                                <!--<a href="{{ route('admin.arsip', 'umum') }}" class="waves-effect">Gobi Surat Masuk-->
                                <!--    Umum</a>-->
                                <a href="{{ route('admin.arsip', 'pemerintahan') }}" class="waves-effect">Gobi Surat
                                    Masuk Pemerintahan</a>

                            </ul>
                        </li>
                        <li>
                            <a href="#" class="has-arrow waves-effect">
                                <i class="ti-upload"></i>
                                <span>Surat Keluar Masyarakat</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                @foreach (lettersAll() as $item)
                                    @if ($item->id == 4)
                                        <a href="{{ route('admin.arsip.out.q', 1) }}" class="waves-effect">Gobi
                                            Surat Keterangan E-KTP</a>
                                        <a href="{{ route('admin.arsip.out.q', 2) }}" class="waves-effect">Gobi
                                            Surat Keterangan Akta Kelahiran</a>
                                    @endif
                                    @if ($item->id != 4)
                                        <a href="{{ route('admin.arsip.out', $item->id) }}"
                                            class="waves-effect">Gobi {{ $item->name }}</a>
                                    @endif
                                @endforeach
                                <a href="{{ route('admin.arsip', 'keluar-pemerintahan') }}" class="waves-effect">Gobi Surat
                                    Keluar Pemerintahan</a>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="menu-title">Master Data</li>
                <li>
                    <a href="{{ route('admin.data.index') }}" class=" waves-effect">

                        <i class="ti-user"></i>
                        <span>Data User</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.users.index') }}" class=" waves-effect">
                        @if (pendingUsers() > 0)
                            <span class="badge badge-pill badge-primary float-right">{{ pendingUsers() }}</span>
                        @endif

                        <i class="ti-agenda"></i>
                        <span>Data Warga</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.letters.index') }}" class=" waves-effect">
                        <i class="ti-receipt"></i>
                        <span>Daftar Surat</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.signatories.index') }}" class=" waves-effect">
                        <i class="ti-user"></i>
                        <span>Data Kepala Desa</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="ti-files"></i>
                        <span>Laporan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li>
                            @foreach (lettersAll() as $item)
                                @if ($item->id == 4)
                                    <a href="{{ route('admin.laporan.q', 1) }}" class=" waves-effect">
                                        <span>Laporan Surat Keterangan E-KTP</span>
                                    </a>
                                    <a href="{{ route('admin.laporan.q', 2) }}" class=" waves-effect">
                                        <span>Laporan Surat Keterangan Akta Kelahiran</span>
                                    </a>
                                @endif
                                @if ($item->id != 4)
                                    <a href="{{ route('admin.laporan', $item->id) }}" class=" waves-effect">
                                        <span>Laporan {{ $item->name }}</span>
                                    </a>
                                @endif
                                
                               
                            @endforeach
                            <!--<a href="{{ route('admin.laporan.masuk', 'umum') }}" class=" waves-effect">-->
                            <!--    <span>Laporan Surat Masuk Umum</span>-->
                            <!--</a>-->
                            <a href="{{ route('admin.laporan.masuk', 'pemerintahan') }}" class=" waves-effect">
                                <span>Laporan Surat Masuk Pemerintahan</span>
                            </a>
                            <a href="{{ route('admin.laporan.masuk', 'keluar-pemerintahan') }}" class=" waves-effect">
                                <span>Laporan Surat Keluar Pemerintahan</span>
                            </a>
                        </li>

                    </ul>
                </li>
                {{-- <li>
                    <a href="{{ route('admin.helpers') }}" class=" waves-effect">
                        <i class="ti-hand-point-right"></i>
                        <span>Helpers</span>
                    </a>
                </li> --}}
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
