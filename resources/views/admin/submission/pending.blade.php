@extends('admin.layouts.master')

@section('title', 'Pengajuan Surat: Menunggu Persetujuan')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Pengajuan Surat: Menunggu Persetujuan
                @endslot
                @slot('li_1')
                    Admin
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Pengajuan Surat: Menunggu Persetujuan</h4>
                    @include('admin.components.message')
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Waktu Pengajuan</th>
                                <th>Nomor Surat</th>
                                <th>Nama</th>
                                <th>Jenis Surat</th>
                                <th>Kepentingan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($submissions as $key => $submission)
                                @php
                                    if ($submission->kepentingan != 'Diri Sendiri') {
                                        $nama_ = json_decode($submission->data)->nama_lengkap;
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $submission->created_at->formatLocalized('%d %B %Y %H:%M') }}</td>
                                    <td>{{ $submission->number ?? '-' }}</td>
                                    <td>{{ $submission->user->name }}</td>
                                    <td>{{ $submission->letter->name }}
                                        {{ $submission->letter->id == 4 ? ($submission->keterangan_pengajuan == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)') : '' }}
                                    </td>
                                    <td>Untuk {{ $submission->kepentingan }}
                                        {{ $submission->kepentingan != 'Diri Sendiri' ? ' : ' . $nama_ : '' }}</td>
                                    <td>
                                        {{-- <form method="POST"
                                            action="{{ route('admin.submissions.print', [$submission->id]) }}"
                                            class="d-inline" target="_blank">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-warning waves-effect waves-light"><i
                                                    class="mdi mdi-printer-check"></i> Cetak</button>
                                        </form> --}}
                                        {{-- <form method="POST"
                                            action="{{ route('admin.submissions.status', [$submission->id, 1]) }}"
                                            class="d-inline form-patch">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light"><i
                                                    class="mdi mdi-format-horizontal-align-right"></i> Setujui</button>
                                        </form>
                                        <button type="submit" class="btn btn-sm btn-danger waves-effect waves-light"
                                            data-toggle="modal" data-target="#modalReject"><i
                                                class="mdi mdi-format-horizontal-align-left"></i> Tolak</button> --}}

                                        {{-- <div class="modal fade" id="modalReject" tabindex="-1" role="dialog"
                                            aria-labelledby="modelTitleId" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Alasan Penolakan</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form method="POST"
                                                        action="{{ route('admin.submissions.status', [$submission->id, 2]) }}">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="">Catatan</label>
                                                                <textarea name="notes" required class="form-control" id="" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Batal</button>
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div> --}}
                                        {{-- <form method="POST"
                                            action="{{ route('admin.submissions.status', [$submission->id, 2]) }}"
                                            class="d-inline form-patch-reject">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-sm btn-danger waves-effect waves-light"><i
                                                    class="mdi mdi-format-horizontal-align-left"></i> Tolak</button>
                                        </form> --}}
                                        <a class="btn btn-sm btn-info waves-effect waves-light"
                                            href="{{ route('admin.submissions.show', $submission->id) }}"
                                            role="button"><i class="mdi mdi mdi-eye-circle"></i> Periksa</a>
                                       
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <!-- Button trigger modal -->

    <!-- Modal -->


@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>

    <script type="text/javascript">
        $(document).on('submit', '.form-patch', function(e) {
            var form = this;
            e.preventDefault();
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes!"
            }).then(function(result) {
                if (result.value) {
                    return form.submit();
                }
            });
        });
        // $(document).on('submit', '.form-patch-reject', function(e) {
        //     $('#modalReject').modal('show');
        // });
    </script>

@endsection
