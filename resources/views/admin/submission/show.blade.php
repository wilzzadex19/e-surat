@extends('admin.layouts.master')

@section('title', 'Detail Pengajuan Surat')

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Detail Pengajuan Surat
                @endslot
                @slot('li_1')
                    Warga
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class='table table-bordered'>
                        <tbody>
                            <tr class='active'>
                                <td colspan="2"><strong><i class='fa fa-bars'></i> Detail Warga yang Mengajukan
                                        Surat</strong></td>
                            </tr>
                            <tr>
                                <td><strong>NIK</strong></td>
                                <td>{{ $submission->user->sin }}</td>
                            </tr>
                            <tr>
                                <td><strong>Nama</strong></td>
                                <td>{{ $submission->user->name }}</td>
                            </tr>
                            <tr>
                                <td><strong>Alamat</strong></td>
                                <td>{{ $submission->user->address ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td><strong>Waktu Pengajuan</strong></td>
                                <td>{{ $submission->created_at->formatLocalized('%d %B %Y %H:%M') }}</td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Surat</strong></td>
                                <td>{{ $submission->letter->name }}
                                    {{ $submission->letter->id == 4 ? ($submission->keterangan_pengajuan == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)') : '' }}
                                </td>
                            </tr>
                            @if ($submission->letter->id == 4)
                                <tr>
                                    <td><strong>Usia</strong></td>
                                    <td>{{ $submission->is_under_age == 1 ? '> Diatas 18 Tahun' : 'Dibawah 18 Tahun' }}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td><strong>Status</strong></td>
                                <td>{{ $submission->getStatus() }}</td>
                            </tr>
                            @if ($submission->approval_status == 2)
                                <tr>
                                    <td><strong>Alasan</strong></td>
                                    <td>{{ $submission->notes }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class='table table-bordered'>
                        <tbody>
                            <tr class='active'>
                                <td colspan="2"><strong><i class='fa fa-bars'></i> Detail Surat Lanjutan</strong></td>
                            </tr>
                            <tr class='active'>
                                {{-- <td colspan="2"><strong>(Masukan Nomor Surat Dahulu Kemudian Save)</strong></td> --}}
                            </tr>
                        </tbody>
                    </table>
                    <form class="custom-validation" method="POST"
                        action="{{ route('admin.submissions.update', $submission->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label for="number" class="col-sm-2 col-form-label">Nomor Surat</label>
                            <div class="col-sm-10">

                                @if ($submission->approval_status == 0)
                                    <input class="form-control" type="text" value="{{ $no_surat }}" readonly>
                                    <small>Nomor Surat diterbitkan jika telah disetujui</small>
                                @elseif($submission->approval_status == 1 || $submission->approval_status == 3)
                                    <input class="form-control" type="text" value="{{ $submission->number ?? '-' }}"
                                        required>
                                @elseif($submission->approval_status == 2)
                                    <input class="form-control" type="text" value="-" required>
                                @endif
                            </div>
                        </div>
                        @foreach ($submission->letter->getData() as $key => $data)
                            <div class="form-group row"
                                style="display: {{ $data->input_type == 'file' ? ($submission->getData($data->input_name . 's') == null ? 'none' : '') : '' }}">
                                <label for="{{ $data->input_name }}"
                                    class="col-sm-2 col-form-label">{{ $data->input_label }}</label>
                                <div class="col-sm-10">
                                    @if ($data->input_required == 'ya')
                                        @switch($data->input_type)
                                            @case('number')
                                                <input class="form-control" type="number" name="{{ $data->input_name }}"
                                                    id="{{ $data->input_name }}"
                                                    value="{{ $submission->getData($data->input_name) }}" required>
                                            @break

                                            @case('textarea')
                                                <textarea name="{{ $data->input_name }}" class="form-control" rows="3" required>{{ $submission->getData($data->input_name) }}</textarea>
                                            @break

                                            @case('date')
                                                {{-- @dump($data->input_name) --}}
                                                <input class="form-control" type="date" name="{{ $data->input_name }}"
                                                    id="{{ $data->input_name }}"
                                                    value="{{ $submission->getData($data->input_name) }}" required>
                                            @break

                                            @case('file')
                                                {{-- @if (is_file(asset($submission->getData($data->input_name . 's')))) --}}

                                                <a href="{{ url($submission->getData($data->input_name . 's')) }}"
                                                    class="btn btn-warning" target="_blank">Lihat File</a>
                                                {{-- @endif --}}
                                            @break

                                            @case('editor')
                                                <textarea id="elm1" name="{{ $data->input_name }}" required>{{ $submission->getData($data->input_name) }}</textarea>
                                            @break

                                            @default
                                                <input class="form-control" type="text" name="{{ $data->input_name }}"
                                                    id="{{ $data->input_name }}"
                                                    value="{{ $submission->getData($data->input_name) }}" required>
                                        @endswitch
                                    @else
                                        <span>Tidak di Diisi</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        @if ($submission->approval_status == 0)
                        <button type="submit" class="btn btn-success waves-effect waves-light mr-1"
                               role="button">Simpan Perubahan</button>
                        @endif
                    </form>
                    <div class="form-group mb-0">
                        <div>

                            {{-- <button type="submit" form="form-print" class="btn btn-warning waves-effect waves-light mr-1"><i
                                    class="mdi mdi-printer-check"></i>
                                Preview</button> --}}


                            <a class="btn btn-secondary waves-effect waves-light float-right mr-1"
                                href="{{ url()->previous() }}" role="button">Kembali</a>
                            @if ($submission->approval_status == 0)
                                <form method="POST"
                                    action="{{ route('admin.submissions.status', [$submission->id, 1]) }}"
                                    class="d-inline form-patch" id="customForm">
                                    @csrf
                                    @method('PATCH')
                                    <button type="button" onclick="submitForms(this)"
                                        class="btn btn-primary waves-effect waves-light float-right mr-1"><i
                                            class="mdi mdi-format-horizontal-align-right"></i> Setujui</button>
                                </form>
                                <button type="submit" class="btn btn-danger waves-effect waves-light float-right mr-1"
                                    data-toggle="modal" data-target="#modalReject"><i
                                        class="mdi mdi-format-horizontal-align-left"></i>
                                    Tolak</button>

                                <div class="modal fade" id="modalReject" tabindex="-1" role="dialog"
                                    aria-labelledby="modelTitleId" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Alasan Penolakan</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="POST"
                                                action="{{ route('admin.submissions.status', [$submission->id, 2]) }}">
                                                @csrf
                                                @method('PATCH')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="">Catatan</label>
                                                        <textarea name="notes" required class="form-control" id="" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    {{-- </form> --}}
                    <form id="form-print" method="POST"
                        action="{{ route('admin.submissions.print', [$submission->id]) }}" class="d-inline form-print"
                        target="_blank">
                        @csrf
                    </form>

                    @foreach ($signatories as $signatory)
                        <form id="form-print-{{ $signatory->id }}" method="POST"
                            action="{{ route('admin.submissions.print', [$submission->id, 'signatory' => $signatory->id]) }}"
                            class="d-inline form-print" target="_blank">
                            @csrf
                        </form>
                    @endforeach
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>


    <script type="text/javascript">
        function submitForms(obj) {
            // alert('tes');
            // Swal.fire({
            //     title: "Are you sure?",
            //     text: "You won't be able to revert this!",
            //     type: "warning",
            //     showCancelButton: true,
            //     confirmButtonColor: "#34c38f",
            //     cancelButtonColor: "#f46a6a",
            //     confirmButtonText: "Yes!"
            // }).then(function(result) {
            //     if (result.value) {
            //         $('#customForm').submit();
            //     }
            // });

            if (confirm('Yakin akan melanjutkan proses ini ?') == true) {
                $('#customForm').submit();
            } else {

            }
        }
        // $(document).on('submit', '.form-patch-reject', function(e) {
        //     $('#modalReject').modal('show');
        // });
    </script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>
    {{-- <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script> --}}



@endsection
