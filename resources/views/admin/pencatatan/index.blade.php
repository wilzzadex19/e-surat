@extends('admin.layouts.master')

@section('title', 'Tambah Data Pencatatan Surat')

@section('content')

<!-- start page title -->
<div class="row align-items-center">
    <div class="col-sm-6">
        @component('admin.components.breadcumb')
        @slot('title') Tambah Pencatatan Surat @endslot
        @slot('li_1') Pencatatan Surat @endslot
        @endcomponent
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Tambah Data Pencatatan Surat</h4>
                <p class="card-title-desc">Silahkan isi dengan benar form dibawah.</p>

                @include('admin.components.message')

                <form class="custom-validation" method="POST" action="{{ route('admin.pencatatan.store') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="sin" class="col-sm-2 col-form-label">Jenis Surat *</label>
                        <div class="col-sm-10">
                           <select name="jenis_surat" class="form-control" required id="jenis_surat">
                               <option value="">- Pilih -</option>
                               <option value="surat-masuk">Surat Masuk</option>
                               <option value="surat-keluar">Surat Keluar</option>
                           </select>
                        </div>
                    </div>
                  
                    <div class="form-group row">
                        <label for="sin" class="col-sm-2 col-form-label">Kategori Surat *</label>
                        <div class="col-sm-10">
                           <select name="kategori_surat" class="form-control" required id="kategori_surat">
                               <option value="">- Pilih -</option>
                               <option value="umum">Umum</option>
                               <option value="pemerintahan">Pemerintahan</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group row b-smasuk b-no_surat">
                        <label for="sin" class="col-sm-2 col-form-label">Nomor Surat *</label>
                        <div class="col-sm-10">
                            <input class="form-control smasuk" type="text" name="no_surat" id="no_surat" value="{{ old('no_surat') }}"
                                data-parsley-type="text" data-parsley-length="[3,255]" required>
                        </div>
                    </div>
                    <div class="form-group row b-skeluar b-letter_id">
                        <label for="sin" class="col-sm-2 col-form-label">Nomor Surat *</label>
                        <div class="col-sm-10">
                           <select onchange="getDate(this)" name="letter_id" class="form-control skeluar" style="width:100%" required id="letter_id">
                               <option value="">- Pilih -</option>
                               @foreach ($submission as $item)
                                   <option data-name="{{ $item->user->name }}" data-tgl-2="{{ date('Y-m-d',strtotime($item->approval_at)) }}" data-tgl="{{ date('Y-m-d',strtotime($item->created_at)) }}" value="{{ $item->id }}">{{ $item->number }} | {{ $item->user->name }}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                   
                   
                    <div class="form-group row">
                        <label for="sin" class="col-sm-2 col-form-label">Sifat Surat *</label>
                        <div class="col-sm-10">
                           <select name="sifat_surat" class="form-control" required id="sifat_surat">
                               <option value="">- Pilih -</option>
                               <option value="penting">Penting</option>
                               <option value="biasa">Biasa</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group row b-skeluar">
                        <label for="sin" class="col-sm-2 col-form-label">Tujuan Surat *</label>
                        <div class="col-sm-10">
                            <input class="form-control skeluar" type="text" name="tujuan_surat" id="tujuan_surat" value="{{ old('tujuan_surat') }}"
                                 data-parsley-length="[3,255]" required>
                        </div>
                    </div>
                    <div class="form-group row b-smasuk">
                        <label for="sin" class="col-sm-2 col-form-label">Asal Surat *</label>
                        <div class="col-sm-10">
                            <input class="form-control smasuk" type="text" name="asal_surat" id="asal_surat" value="{{ old('asal_surat') }}" required>
                        </div>
                    </div>
                    <div class="form-group row b-smasuk">
                        <label for="name" class="col-sm-2 col-form-label">Tanggal Surat Diterima *</label>
                        <div class="col-sm-10">
                            <input class="form-control smasuk" data-parsley-type="date" type="date" name="tgl_surat_diterima" id="tgl_surat_diterima" value="{{ old('tgl_surat_diterima') }}" required>
                        </div>
                    </div>
                    <div class="form-group row b-skeluar">
                        <label for="name" class="col-sm-2 col-form-label">Tanggal Dikeluarkan *</label>
                        <div class="col-sm-10">
                            <input class="form-control skeluar" data-parsley-type="date" type="date" name="tgl_surat_dikeluarkan" id="tgl_surat_dikeluarkan" value="{{ old('tgl_surat_dikeluarkan') }}" required>
                        </div>
                    </div>
                    <div class="form-group row b-skeluar">
                        <label for="name" class="col-sm-2 col-form-label">Tanggal Surat*</label>
                        <div class="col-sm-10">
                            <input class="form-control skeluar" data-parsley-type="date" type="date" name="tgl_surat" id="tgl_surat" value="{{ old('tgl_surat') }}" required>
                        </div>
                    </div>
                   
                    <div class="form-group row b-smasuk">
                        <label for="sin" class="col-sm-2 col-form-label">Diterima Oleh *</label>
                        <div class="col-sm-10">
                           <select name="diterima_oleh" class="form-control smasuk" required id="diterima_oleh">
                               <option value="">- Pilih -</option>
                               <option value="Kepala Desa">Kepala Desa</option>
                               <option value="Kaur Umum">Kaur Umum</option>
                               <option value="Sekretaris Desa">Sekretaris Desa</option>
                           </select>
                        </div>
                    </div>
                    <div class="form-group row b-skeluar">
                        <label for="sin" class="col-sm-2 col-form-label">Dikeluarkan Oleh *</label>
                        <div class="col-sm-10">
                           <select name="dikeluarkan_oleh" class="form-control skeluar" required id="dikeluarkan_oleh">
                               <option value="">- Pilih -</option>
                               <option value="Kepala Desa">Kepala Desa</option>
                               <option value="Kaur Umum">Kaur Umum</option>
                               <option value="Sekretaris Desa">Sekretaris Desa</option>
                           </select>
                        </div>
                    </div>  
                    <div class="form-group row b-smasuk">
                        <label for="sin" class="col-sm-2 col-form-label">Perihal Surat*</label>
                        <div class="col-sm-10">
                            <input class="form-control smasuk" type="text" name="perihal" id="perihal" value="{{ old('perihal') }}"
                                 data-parsley-length="[3,255]" required>
                        </div>
                    </div>
                    {{-- <div class="form-group row b-smasuk">
                        <label for="sin" class="col-sm-2 col-form-label">Isi Ringkas *</label>
                        <div class="col-sm-10">
                            <textarea name="isi_ringkas" required class="form-control smasuk" cols="30" rows="10">{{ old('isi_ringkas') }}</textarea>
                        </div>
                    </div> --}}
                   
                   
                    <div class="form-group row">
                        <label for="sin" class="col-sm-2 col-form-label">Lampiran *</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="file" accept="application/pdf,image/jpg,image/png,image/jpeg" name="lampiran" id="lampiran" value="{{ old('tujuan_surat') }}"
                                 required>
                        </div>
                    </div>
                   
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light"
                                href="{{ route('admin.signatories.index') }}" role="button">Cancel</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

@endsection

@section('script')
<!-- Plugins js -->
<script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>

<script>
    $('#letter_id').select2();
    $('#jenis_surat').on('change',function(){
        let jenis_surat = $(this).find(':selected').val();


        if(jenis_surat == 'surat-masuk'){
            $('.skeluar').prop('required',false);
            $('.smasuk').prop('required',true);
            $('.b-smasuk').show();
            $('.b-skeluar').hide();

        }else{


            $('.skeluar').prop('required',true);
            $('.smasuk').prop('required',false);
            $('.b-smasuk').hide();
            $('.b-skeluar').show();
        }


    });

    $('#kategori_surat').on('change',function(){
        let kategori_surat = $(this).find(':selected').val();
        if(kategori_surat == 'pemerintahan'){
            $('#no_surat').prop('required',true);
            $('#letter_id').prop('required',false);
            $('.b-no_surat').show();
            $('.b-letter_id').hide();

        }else{
            $('#no_surat').prop('required',false);
            $('#letter_id').prop('required',true);
            $('.b-no_surat').hide();
            $('.b-letter_id').show();
        }
    });

    function getDate(o){
        let tanggal = $(o).find(':selected').attr('data-tgl');
        let tanggal2 = $(o).find(':selected').attr('data-tgl-2');
        let nama = $(o).find(':selected').attr('data-name');
        // console.log(nama)
        
        $('#tgl_surat_dikeluarkan').val(tanggal2)
        $('#tgl_surat').val(tanggal)
        $('#tujuan_surat').val(nama)
    }
</script>
@endsection