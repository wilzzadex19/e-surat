@extends('admin.layouts.master')

@section('title', '')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Arsip Surat Masuk : {{ $label }}
                @endslot
                @slot('li_1')
                    Admin
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- <h4 class="card-title">Pengajuan Surat: Menunggu Persetujuan</h4> --}}
                    @include('admin.components.message')
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Surat</th>
                                {{-- <th>Tanggal Surat</th> --}}
                                <th>Tanggal Surat Diterima</th>
                                <th>Sifat Surat</th>
                                <th>Diterima Oleh</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($arsip as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->letter_number }}</td>
                                    {{-- <td>{{ date('d M Y', strtotime($item->letter_date)) }}</td> --}}
                                    <td>{{ date('d M Y', strtotime($item->letter_date_receive)) }}</td>
                                    <td>{{ ucfirst($item->letter_trait) }}</td>
                                    <td>{{ ucfirst($item->receive_by) }}</td>
                                    <td>
                                        <a href="{{ route('admin.arsip.detail',$item->id) }}" class="btn btn-sm btn-primary waves-effect waves-light"><i
                                                class="mdi mdi-eye-circle"></i> Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <!-- Button trigger modal -->

    <!-- Modal -->


@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>

    <script type="text/javascript">
        $(document).on('submit', '.form-patch', function(e) {
            var form = this;
            e.preventDefault();
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes!"
            }).then(function(result) {
                if (result.value) {
                    return form.submit();
                }
            });
        });
        // $(document).on('submit', '.form-patch-reject', function(e) {
        //     $('#modalReject').modal('show');
        // });
    </script>

@endsection
