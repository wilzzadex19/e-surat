@extends('admin.layouts.master')

@section('title', 'Detail Pengajuan Surat')

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Detail Arsip Surat
                @endslot
                @slot('li_1')
                    Admin
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class='table table-bordered'>
                        <tbody>
                            <tr class='active'>
                                <td colspan="2"><strong><i class='fa fa-bars'></i> Detail Arsip Surat Bernomor
                                        {{ $arsip->submission->number }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Jenis Surat</strong></td>
                                <td>{{ $arsip->letter_type == 'surat-masuk' ? 'Surat Masuk' : 'Surat Keluar' }}</td>
                            </tr>
                            <tr>
                                <td><strong>Nama Surat</strong></td>
                                <td>{{ $arsip->letter->name }} {{ $arsip->letter->id == 4 ? $arsip->submission->keterangan_pengajuan == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)' : ''  }}</td>
                            </tr>
                            <tr>
                                <td><strong>Kategori Surat</strong></td>
                                <td>{{ ucfirst($arsip->letter_category) }}</td>
                            </tr>
                            <tr>
                                <td><strong>No Surat</strong></td>
                                <td>{{ $arsip->submission->number }}</td>
                            </tr>
                            <tr>
                                <td><strong>Tanggal Dikeluarkan</strong></td>
                                <td>{{ date('d M Y', strtotime($arsip->letter_date_out)) }}</td>
                            </tr>

                            <tr>
                                <td><strong>Penerima</strong></td>
                                <td>{{ $arsip->submission->user->name }}</td>
                            </tr>
                            
                            @if ($arsip->file != null)
                                <tr>
                                    <td><strong>Lampiran</strong></td>
                                    <td><a href="{{ asset($arsip->file) }}" target="_blank"><u>Lihat Lampiran</u></a></td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="2">
                                    <form method="POST" action="{{ route('admin.submissions.print', [$arsip->submission->id]) }}"
                                        class="d-inline" target="_blank">
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-warning waves-effect waves-light"><i
                                                class="mdi mdi-printer-check"></i> Cetak</button>
                                    </form>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                    <a href="{{ url()->previous() }}" class="float-right btn btn-warning">Kembali</a>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/tinymce/tinymce.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>

@endsection
