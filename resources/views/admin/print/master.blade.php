@if ($submission->letter->id == 5)
	
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>Pengajuan {{ $submission->letter->name }} oleh {{ $submission->user->name }} | {{ config('app.name') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
	<meta content="Themesbrand" name="author" />
	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ URL::asset('assets/logo1.png') }}">
	<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" id="bootstrap-light" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body {
			width: 100%;
			height: 100%;
			margin: 0 auto;
			background-color: #FAFAFA;
			font-family: "Times New Roman";
			font-size: 17px !important;
			color: black;
		}
		* {
			box-sizing: border-box;
			-moz-box-sizing: border-box;
		}

		page {
			background: white;
			display: block;
			margin: 0 auto;
		}

		page[size="A5"] {
			width: 21cm;
		}

		@media print {
			html, body {
				width: 148mm;
				height: 210mm;
				margin: 0;
				padding: 0;
			}
		}
	</style>
</head>

<body>
	<page size="A5">
		{{-- <div class="row">
			<div class="col-2">
				<img class="mt-5" src="{{ asset('assets/logo1.png') }}" width="150px">
			</div>
			<div class="col-8">
				<h4 class="mt-5 text-center">PEMERINTAH KABUPATEN SAMBAS</h4>
				<h4 class="mt-2 text-center">KANTOR DESA PARIT BARU</h4>
				<h5 class="mt-2 text-center">KECAMATAN SELAKAU</h5>
				<p class="text-center mb-0">Alamat: Jl. Raya Parit Baru Selakau, Kode Pos: 79452</p>
			</div>
			
		</div>
		<hr style="border:2px solid black"> --}}
		<div class="pl-3 pr-3 mt-3">
			<div class="row">
				<div class="col-12">
					<h4 class="text-center" style="margin-top: 50px"> <strong><u>{{ strtoupper($submission->letter->name) }}</u></strong> </h4>
					<p class="text-center">NOMOR: {{ $submission->number ?? '-' }}</p>
				</div>
			</div>
			<div class="row" style="line-height: 1.7;text-align: justify;">
				<div class="col-12">
					@yield('content')
				</div>
			</div>
		</div>
	</page>
	<script src="{{ URL::asset('assets/libs/jquery/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('assets/libs/bootstrap/bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		window.print();
	</script>
</body>

</html>
@else

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>Pengajuan {{ $submission->letter->name }} oleh {{ $submission->user->name }} | {{ config('app.name') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
	<meta content="Themesbrand" name="author" />
	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ URL::asset('assets/logo1.png') }}">
	<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" id="bootstrap-light" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body {
			width: 100%;
			height: 100%;
			margin: 0 auto;
			background-color: #FAFAFA;
			font-family: "Times New Roman";
			font-size: 17px !important;
			color: black;
		}
		* {
			box-sizing: border-box;
			-moz-box-sizing: border-box;
		}

		page {
			background: white;
			display: block;
			margin: 0 auto;
		}

		page[size="A4"] {
			width: 23cm;
		}

		@media print {
			html, body {
				width: 210mm;
				height: 297mm;
				margin: 0;
				padding: 0;
			}
		}
	</style>
</head>

<body>
	<page size="A4">
		<div class="row">
			<div class="col-2">
				<img class="mt-5" src="{{ asset('assets/logo1.png') }}" width="150px">
			</div>
			<div class="col-8">
				<h4 class="mt-5 text-center">PEMERINTAH KABUPATEN SAMBAS</h4>
				<h4 class="mt-2 text-center">KANTOR DESA PARIT BARU</h4>
				<h5 class="mt-2 text-center">KECAMATAN SELAKAU</h5>
				<p class="text-center mb-0">Alamat: Jl. Raya Parit Baru Selakau, Kode Pos: 79452</p>
			</div>
			
		</div>
		<hr style="border:2px solid black">
		<div class="pl-3 pr-3 mt-3">
			<div class="row">
				<div class="col-12">
					<h4 class="text-center"> <strong><u>{{ strtoupper($submission->letter->name) }}</u></strong> </h4>
					<p class="text-center">NOMOR: {{ $submission->number ?? '-' }}</p>
				</div>
			</div>
			<div class="row" style="line-height: 1.7;text-align: justify;">
				<div class="col-12">
					@yield('content')
				</div>
			</div>
		</div>
	</page>
	<script src="{{ URL::asset('assets/libs/jquery/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('assets/libs/bootstrap/bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		window.print();
	</script>
</body>

</html>

@endif