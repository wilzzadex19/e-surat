@extends('admin.layouts.master')

@section('title', 'Tambah Surat')

@section('content')
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Tambah Surat
                @endslot
                @slot('li_1')
                    Surat
                @endslot
            @endcomponent
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Pengajuan Surat Untuk Warga</h4>
                    <p class="card-title-desc">Silahkan Klik Form Surat Yang Tersedia & Isi Dengan Benar</p>
                    @include('components.message')
                    <div id="accordion">
                        @foreach ($letters as $letter)
                            <div class="card mb-1">
                                <div class="card-header p-3" id="heading{{ $letter->id }}">
                                    <h6 class="m-0 font-14">
                                        <a href="#collapse{{ $letter->id }}" class="text-dark" data-toggle="collapse"
                                            aria-expanded="true"
                                            aria-controls="collapse{{ $letter->id }}">{{ $letter->name }}</a>
                                    </h6>
                                </div>

                                <div id="collapse{{ $letter->id }}" class="collapse"
                                    aria-labelledby="heading{{ $letter->id }}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            {!! $letter->information !!}
                                        </div>
                                        <form class="custom-validation" method="POST"
                                            action="{{ route('admin.formsurat.store', $letter->id) }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="user_id" class="col-sm-2 col-form-label">Kepentingan
                                                    Surat</label>
                                                <div class="col-sm-10">
                                                    <select name="kepentingan_surat"
                                                        id="kepentingan_surat_{{ $letter->id }}" class="form-control"
                                                        required style="width:100%">
                                                        <option value="">- Pilih -</option>
                                                        <option value="Diri Sendiri">Untuk Diri Sendiri</option>
                                                        <option value="Orang Lain">Untuk Orang Lain</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_id" class="col-sm-2 col-form-label">Warga</label>
                                                <div class="col-sm-10">
                                                    <select name="user_id" onchange="generateUser(this)"
                                                        class="form-control select2" required style="width:100%">
                                                        <option value="">- Pilih Warga -</option>
                                                        @foreach ($warga as $item)
                                                            <option value="{{ $item->id }}"
                                                                data-nik="{{ $item->sin }}"
                                                                data-nama="{{ $item->name }}"
                                                                data-tempat_lahir="{{ $item->birth_place }}"
                                                                data-tanggal_lahir="{{ $item->birth_date }}"
                                                                data-jenis_kelamin="{{ $item->gender }}"
                                                                data-status_perkawinan="{{ $item->marital_status }}"
                                                                data-agama="{{ $item->religion }}"
                                                                data-pekerjaan="{{ $item->profession }}"
                                                                data-alamat="{{ $item->address }}"
                                                                data-letter="{{ $letter->id }}">{{ $item->sin }} -
                                                                {{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @if ($letter->id == 4)
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Keterangan
                                                        Pengajuan</label>
                                                    <div class="col-sm-10">
                                                        <select name="keterangan_pengajuan"
                                                            onchange="getSyarat(this,'{{ $letter->id }}')" required
                                                            class="form-control">
                                                            <option value="">- Pilih -</option>
                                                            <option value="1">Belum Pernah Melakukan Perekaman E-KTP
                                                            </option>
                                                            <option value="2">Belum Memiliki Akta Kelahiran</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row b-usia_syarat" style="display: none">
                                                    <label for="" class="col-sm-2 col-form-label">Usia</label>
                                                    <div class="col-sm-10">
                                                        <select name="usia_syarat" id="usia_syarat"
                                                            onchange="getSyaratFile(this,'{{ $letter->id }}')"
                                                            class="form-control">
                                                            <option value="">- Pilih -</option>
                                                            <option value="1">Usia 18 Tahun
                                                            </option>
                                                            <option value="0">Dibawah 18 Tahun</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                            @foreach (json_decode($letter->data) as $data)
                                                @if ($data->input_type != 'file')
                                                    <div
                                                        class="form-group row bparent_{{ $letter->id }}_{{ !empty($data->is_special) ? ($data->is_special == 'ya' ? $data->parents : '') : '' }}">
                                                        <label for="{{ $data->input_name }}"
                                                            class="col-sm-2 col-form-label">{{ $data->input_label }}</label>
                                                        <div class="col-sm-10">

                                                            @switch($data->input_type)
                                                                @case('number')
                                                                    <input class="form-control"
                                                                        placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                        type="number" name="{{ $data->input_name }}"
                                                                        id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                        value="{{ old($data->input_name) }}"
                                                                        {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                @break

                                                                @case('text')
                                                                    <input class="form-control"
                                                                        placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                        type="text" name="{{ $data->input_name }}"
                                                                        id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                        value="{{ old($data->input_name) }}"
                                                                        {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                @break

                                                                @case('file')
                                                                    {{-- <input class="form-control input_{{ $letter->id }}_{{ !empty($data->is_special) ? ($data->is_special == 'ya' ? $data->parents : '') : '' }}"
                                                                    placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                    type="file" accept="application/pdf, image/jpeg, image/png"
                                                                    name="{{ $data->input_name }}"
                                                                    id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                    value="{{ old($data->input_name) }}"
                                                                    {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                <small>Format yang didukung : jpg,png,pdf</small> --}}
                                                                @break

                                                                @case('date')
                                                                    <input class="form-control"
                                                                        placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}"
                                                                        type="date" name="{{ $data->input_name }}"
                                                                        id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                        value="{{ old($data->input_name) }}"
                                                                        {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}>
                                                                    {{-- <small>Format yang didukung : jpg,png,pdf</small> --}}
                                                                @break

                                                                @default
                                                                    <textarea name="{{ $data->input_name }}" id="{{ $data->input_name }}_{{ $letter->id }}"
                                                                        {{ !empty($data->input_required) ? ($data->input_required == 'ya' ? 'required' : '') : '' }}
                                                                        placeholder="{{ !empty($data->input_placeholder) ? $data->input_placeholder : '' }}" class="form-control"
                                                                        rows="3">{{ old($data->input_name) }}</textarea>
                                                            @endswitch

                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach


                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit"
                                                        class="btn btn-primary waves-effect waves-light mr-1">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.select2').select2();

        })

        function getSyarat(onbj, letter_id) {
            let keterangan_pengajuan = $(onbj).find(':selected').val();
            // console.log(letter_id);
            // console.log(keterangan_pengajuan);
            if (keterangan_pengajuan == 2) {
                $('.b-usia_syarat').show();
                $('#usia_syarat').prop('required', true);

            } else {
                $('.b-usia_syarat').hide();
                $('#usia_syarat').prop('required', false);
                $('.input_' + letter_id + '_diatas').prop('required', false);
                $('.input_' + letter_id + '_dibawah').prop('required', false);
                $('.bparent_' + letter_id + '_diatas').hide();
                $('.bparent_' + letter_id + '_dibawah').hide();
            }
        }

        function getSyaratFile(obj, letter_id) {
            console.log('.bparent_' + letter_id + '_diatas')
            let syarat = $(obj).find(':selected').val();
            if (syarat == 1) {
                // console.log('tes')
                $('.input_' + letter_id + '_diatas').prop('required', true);
                $('.input_' + letter_id + '_dibawah').prop('required', false);
                $('.bparent_' + letter_id + '_diatas').show();
                $('.bparent_' + letter_id + '_dibawah').hide();
            } else {
                $('.input_' + letter_id + '_diatas').prop('required', false);
                $('.input_' + letter_id + '_dibawah').prop('required', true);
                $('.bparent_' + letter_id + '_diatas').hide();
                $('.bparent_' + letter_id + '_dibawah').show();
            }
        }

        function generateUser(obj) {
            let letter_id = $(obj).find(':selected').data('letter');
            let kepentingan = $('#kepentingan_surat_' + letter_id).find(':selected').val();
            if (kepentingan == 'Diri Sendiri') {

                let nama_lengkap = $(obj).find(':selected').data('nama');
                let tempat_lahir = $(obj).find(':selected').data('tempat_lahir');
                let tanggal_lahir = $(obj).find(':selected').data('tanggal_lahir');
                let jenis_kelamin = $(obj).find(':selected').data('jenis_kelamin');
                let status_perkawinan = $(obj).find(':selected').data('status_perkawinan');
                let agama = $(obj).find(':selected').data('agama');
                let pekerjaan = $(obj).find(':selected').data('pekerjaan');
                let alamat = $(obj).find(':selected').data('alamat');
                let nik = $(obj).find(':selected').data('nik');
                // console.log(nama_lengkap,tempat_lahir,tanggal_lahir,jenis_kelamin,status_perkawinan,agama,pekerjaan,alamat);
                $('#nama_lengkap_' + letter_id).val(nama_lengkap);
                $('#tempat_lahir_' + letter_id).val(tempat_lahir);
                $('#tanggal_lahir_' + letter_id).val(tanggal_lahir.replace(" 00:00:00", ''));
                $('#jenis_kelamin_' + letter_id).val(jenis_kelamin);
                $('#status_perkawinan_' + letter_id).val(status_perkawinan);
                $('#agama_' + letter_id).val(agama);
                $('#pekerjaan_' + letter_id).val(pekerjaan);
                $('#alamat_' + letter_id).val(alamat);
                $('#nik_' + letter_id).val(nik);
                $('#alamat_' + letter_id).text(alamat);
                $('#kewarganegaraan_' + letter_id).val('WNI');
            } else {
                $('#nama_lengkap_' + letter_id).val('');
                $('#tempat_lahir_' + letter_id).val('');
                $('#tanggal_lahir_' + letter_id).val('');
                $('#jenis_kelamin_' + letter_id).val('');
                $('#status_perkawinan_' + letter_id).val('');
                $('#agama_' + letter_id).val('');
                $('#pekerjaan_' + letter_id).val('');
                $('#alamat_' + letter_id).val('');
                $('#alamat_' + letter_id).text('');
                $('#nik_' + letter_id).val('');
                $('#kewarganegaraan_' + letter_id).val('');
            }
        }
    </script>

@endsection
