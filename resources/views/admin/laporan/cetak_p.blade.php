<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $judul }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @media print {
            @page {
                size: auto;
                margin-top: 0;
                margin-bottom: 0px;
            }

            #data,
            #data th,
            #data td {
                border: 1px solid;
            }

            #data td,
            #data th {
                padding: 5px;
            }

            #data {
                border-spacing: 0px;
                margin-top: 40px;
                font-size: 17px;
            }

            #childTable {
                border: none;
            }

            body {
                padding-top: 10px;
                font-family: sans-serif;
            }
        }
    </style>
</head>

<body onload="window.print()">
    <table style="width:100%;margin-top: 50px">
        <tr>
            <td style="width: 100%" colspan="3">
                <div class="row">
                    <div class="col-3 text-center">
                        <img width="70%" src="{{ URL::asset('assets/logo1.png') }}""
                            alt="">
                    </div>
                    <div class=" col-6">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4> <strong>Sistem Informasi Pelayanan Administrasi Surat Menyurat Desa Parit Baru
                                    </strong></h4>
                                <h5><strong>{!! $judul !!}</strong></h5>
                            </div>
                            <div class="col-12">
                                {{-- No. {{ $data->no_faktur }} --}}
                            </div>
                            <div class="col-12 text-center">
                                <span>Jl. Raya Parit Baru Selakau, Kode Pos: 79452</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">

                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-6 text-center">
                        <span>{{ config('global.app_setting')->app_name }}</span>
                    </div>
                </div> --}}
            </td>

        </tr>
    </table>

    <hr style="border: 1px solid black">

    <table id="data" style="width:100%">
        <tr>
            <td class="text-center">NO.</td>
            <td class="text-center">NO SURAT</td>
            <td class="text-center">TANGGAL SURAT DIKELUARKAN</td>
            {{-- <td class="text-center">Suplier</td> --}}
            <td class="text-center">SIFAT SURAT</td>
            <td class="text-center">DIKELUARKAN OLEH</td>
            <td class="text-center">TUJUAN SURAT</td>
        </tr>
        @php
            $total_pemasukan = 0;
        @endphp
        @foreach ($submissions as $key => $submission)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $submission->letter_number ?? '-' }}</td>
                <td>{{ $submission->created_at->formatLocalized('%d %B %Y %H:%M') }}</td>
                <td>{{ $submission->letter_trait }}</td>
                <td>{{ $submission->out_by }}</td>
                <td>{{ $submission->to }}</td>
            </tr>
        @endforeach
       
    </table>
    <br>
    <br>
    <table style="width: 100%; text-align:center" border="0">
        <tr>
            <td style="width: 30%"></td>
            <td style="width:40%"></td>
            <td style="width:30%; text-align: center">Parit Baru, {{ date('d M Y') }}</td>
        </tr>
        <tr>
            <td style="width: 30%"></td>
            <td style="width:40%"></td>
            <td style="width:30%; text-align: center">Kepala Desa Parit Baru</td>
           
        </tr>
        <tr>
            <td style="width: 30%"></td>
            <td style="width:40%"></td>
            <td style="width:30%; text-align: center">Kecamatan Selakau</td>           
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><br><br><br></td>
        </tr>
        <tr>
            <td style="width: 30%"></td>
            <td style="width:40%"></td>
            <td style="width:30%; text-align: center"><strong>SUHARDI</strong></td>           
        </tr>
    </table>
</body>

</html>
