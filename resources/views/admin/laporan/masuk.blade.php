@extends('admin.layouts.master')

@section('title', 'Pengajuan Surat: Menunggu Persetujuan')

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('admin.components.breadcumb')
                @slot('title')
                    Laporan Surat Masuk : {{ $label }}
                @endslot
                @slot('li_1')
                    Kepala Desa
                @endslot
            @endcomponent
        </div>
        <div class="col-sm-6">
            <div class="float-right d-md-block mb-3">
                <button class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="modal"
                    data-target="#modalCetak" type="button">
                    <i class="mdi mdi-gesture-spread mr-2"></i> Cetak Laporan
                </button>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- <h4 class="card-title">Pengajuan Surat: Menunggu Persetujuan</h4> --}}
                    @include('admin.components.message')
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Surat</th>
                                {{-- <th>Tanggal Surat</th> --}}
                                <th>Tanggal Surat Diterima</th>
                                <th>Sifat Surat</th>
                                <th>Diterima Oleh</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($arsip as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->letter_number }}</td>
                                    {{-- <td>{{ date('d M Y', strtotime($item->letter_date)) }}</td> --}}
                                    <td>{{ date('d M Y', strtotime($item->letter_date_receive)) }}</td>
                                    <td>{{ ucfirst($item->letter_trait) }}</td>
                                    <td>{{ ucfirst($item->receive_by) }}</td>
                                    {{-- <td>
                                        <a href="{{ route('admin.arsip.detail', $item->id) }}"
                                            class="btn btn-sm btn-primary waves-effect waves-light"><i
                                                class="mdi mdi-eye-circle"></i> Detail</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="modalCetak" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.laporan.cetak.masuk') }}" target="_blank" method="POST">
                    @csrf
                    <div class="modal-body">
                        <label>Pilih Periode</label>
                        <input type="hidden" name="jenis" value="{{ $jenis }}">
                        <div id="reportrange"
                            style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                        <input type="hidden" name="tanggal" id="tanggal">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Cetak</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


    <script type="text/javascript">
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#tanggal').val(start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'))
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf(
                        'month')
                ]
            }
        }, cb);

        cb(start, end);
    </script>

    <script type="text/javascript">
        $(document).on('submit', '.form-patch', function(e) {
            var form = this;
            e.preventDefault();
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#34c38f",
                cancelButtonColor: "#f46a6a",
                confirmButtonText: "Yes!"
            }).then(function(result) {
                if (result.value) {
                    return form.submit();
                }
            });
        });
        // $(document).on('submit', '.form-patch-reject', function(e) {
        //     $('#modalReject').modal('show');
        // });
    </script>

@endsection
