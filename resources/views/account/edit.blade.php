@extends('layouts.master')

@section('title', 'Data Diri: ' . $user->name)

@section('css')
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            @component('components.breadcumb')
                @slot('title')
                    Data Diri: {{ $user->name }}
                @endslot
                @slot('li_1')
                    Home
                @endslot
            @endcomponent
        </div>
    </div>
    <!-- end page title -->

    {{-- <div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Nomor Whatsapp</h4>
                <p class="card-title-desc">Dapatkan pemberitahun mengenai pengajuan surat melalui Whatsapp. ex: 6281234567890 (jika format salah, pesan tidak akan terkirim)</p>

                @include('components.message')

                <form class="custom-validation" method="POST" action="{{ route('account.whatsapp') }}"
                    enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="phone_number" class="col-sm-2 col-form-label">Nomor Whatsapp</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" name="phone_number" id="phone_number"
                                value="{{ old('phone_number', $user->phone_number ?? '628') }}" data-parsley-length="[5,255]" required>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row --> --}}

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Data Akun</h4>
                    <p class="card-title-desc">Silahkan edit dengan benar form dibawah.</p>

                    @include('components.message')
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong>Selamat!</strong> Perubahan berhasil di simpan.
                        </div>
                    @endif


                    <form class="custom-validation" method="POST" action="{{ route('account.update') }}"
                        enctype="multipart/form-data">
                        {{-- @dump($user) --}}
                        @csrf
                        {{-- @method('PATCH') --}}
                        <div class="form-group row">
                            <label for="photo" class="col-sm-3 col-form-label">Foto</label>
                            <div class="col-sm-9">
                                <a class="image-popup-no-margins" href="{{ asset($user->photo) }}">
                                    <img class="img-fluid" alt="{{ $user->name }}" src="{{ asset($user->photo) }}"
                                        width="120">
                                </a>
                                <input class="form-control mt-2" type="file" name="photo" id="photo">
                                <p class='help-block'>Please leave empty if not change.</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sin" class="col-sm-3 col-form-label">NIK *</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $user->sin }}" name="sin" id="sin"
                                    data-parsley-type="number" data-parsley-length="[16,255]" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama *</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" value="{{ $user->name }}" name="name" id="name"
                                    data-parsley-length="[5,255]" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Email *</label>
                            <div class="col-sm-9">
                                <input class="form-control" value="{{ $user->email }}" type="text" name="email"
                                    id="email" data-parsley-length="[5,255]" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="birth_place" class="col-sm-3 col-form-label">Tempat Lahir *</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="birth_place" id="birth_place"
                                    value="{{ $user->birth_place }}" data-parsley-length="[5,255]" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="birth_date" class="col-sm-3 col-form-label">Tanggal Lahir *</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" name="birth_date" id="birth_date"
                                    value="{{ date('Y-m-d', strtotime($user->birth_date)) }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gender" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="gender" id="gender" required>
                                    <option selected disabled>Select *</option>
                                    <option {{ $user->gender == 'Laki - Laki' ? 'selected' : '' }} value="Laki - Laki">
                                        Laki
                                        - Laki</option>
                                    <option {{ $user->gender == 'Perempuan' ? 'selected' : '' }} value="Perempuan">
                                        Perempuan
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea id="address" name="address" class="form-control" maxlength="225" rows="3">{{ $user->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="religion" class="col-sm-3 col-form-label">Agama</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="religion" id="religion" required>
                                    <option selected disabled>Select *</option>
                                    <option {{ $user->religion == 'Islam' ? 'selected' : '' }} value="Islam">Islam
                                    </option>
                                    <option {{ $user->religion == 'Protestan' ? 'selected' : '' }} value="Protestan">
                                        Protestan</option>
                                    <option {{ $user->religion == 'Katolik' ? 'selected' : '' }} value="Katolik">Katolik
                                    </option>
                                    <option {{ $user->religion == 'Hindu' ? 'selected' : '' }} value="Hindu">Hindu
                                    </option>
                                    <option {{ $user->religion == 'Buddha' ? 'selected' : '' }} value="Buddha">Buddha
                                    </option>
                                    <option {{ $user->religion == 'Konghucu' ? 'selected' : '' }} value="Konghucu">
                                        Konghucu
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="marital_status" class="col-sm-3 col-form-label">Status
                                Perkawinan</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="marital_status" id="marital_status" required>
                                    <option selected disabled>Select *</option>
                                    <option {{ $user->marital_status == 'Belum Kawin' ? 'selected' : '' }}
                                        value="Belum Kawin">Belum Kawin</option>
                                    <option {{ $user->marital_status == 'Kawin' ? 'selected' : '' }} value="Kawin">Kawin
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="profession" class="col-sm-3 col-form-label">Pekerjaan *</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="profession" id="profession"
                                    value="{{ $user->profession }}" data-parsley-length="[5,255]" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone_number" class="col-sm-3 col-form-label">Phone Number</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="phone_number" id="phone_number"
                                    value="{{ $user->phone_number }}" data-parsley-length="[3,255]">
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Update
                                </button>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/lightbox.init.js') }}"></script>

@endsection
