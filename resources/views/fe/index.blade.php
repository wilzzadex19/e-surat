<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Sistem E-Surat Pemerintah Desa Parit Baru</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('assets/logo1.png') }}" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet"
        type="text/css" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('frontend') }}/css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        .float {
            position: fixed;
            top: 100px;
            right: 10px;
            padding: 5px;
            background-color: #25d366;
            color: #FFF;
            padding-left: 10px;
            border-radius: 20px 5px 5px 20px;
            text-align: center;
            font-size: 18px;
            font-weight: bold;
            box-shadow: 2px 2px 3px #999;
            z-index: 100;
            border: none;
        }

        .float2 {
            position: fixed;
            top: 150px;
            right: 10px;
            padding: 5px;
            background-color: #25d366;
            color: #FFF;
            padding-left: 10px;
            border-radius: 20px 5px 5px 20px;
            text-align: center;
            font-size: 18px;
            font-weight: bold;
            box-shadow: 2px 2px 3px #999;
            z-index: 100;
            border: none;
        }

        .my-float {
            margin-top: 16px;
        }


        @media only screen and (max-width: 1000px) {
            #box-map {
                display: none;
            }

            #gmap_canvas {
                display: none;
            }
        }

        @media only screen and (max-width: 429px) {
            .float {
                position: fixed;
                top: 120px;
                right: 10px;
                padding: 5px;
                background-color: #25d366;
                color: #FFF;
                padding-left: 10px;
                border-radius: 20px 5px 5px 20px;
                text-align: center;
                font-size: 18px;
                font-weight: bold;
                box-shadow: 2px 2px 3px #999;
                z-index: 100;
                border: none;
            }

            .float2 {
                position: fixed;
                top: 170px;
                right: 10px;
                padding: 5px;
                background-color: #25d366;
                color: #FFF;
                padding-left: 10px;
                border-radius: 20px 5px 5px 20px;
                text-align: center;
                font-size: 18px;
                font-weight: bold;
                box-shadow: 2px 2px 3px #999;
                z-index: 100;
                border: none;
            }
        }


        .navbar-inverse {
            background-color: transparent;
            border-color: transparent;
        }

        .navbar-inverse .navbar-brand {
            color: #fff;
        }

        .navbar-inverse .navbar-nav>li>a {
            color: #fff;
            text-transform: uppercase;
            font-weight: 900;
        }

        header .carousel-inner .item {
            height: 100vh;
        }

        header .carousel-inner .item img {
            width: 100%;
        }

        .carousel-caption {
            padding-bottom: 250px;
        }

        .carousel-caption h2 {
            font-size: 50px;
            text-transform: uppercase;
        }

        .carousel-control.right {
            background-image: none;
        }

        .carousel-control.left {
            background-image: none;
        }
    </style>

</head>

<body>

    <!-- Navigation-->
    <nav class="navbar navbar-light bg-light fixed-top" style="background-color: #4F5551 !important">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('frontend/logo.jpg') }}" width="200" alt="">
            </a>

            <div class="div">
                <a class="btn btn-success" href="{{ route('login') }}">Login</a>
                <a class="btn btn-success" href="{{ route('registrasi') }}">Registrasi</a>
            </div>

        </div>
    </nav>
    <!-- Masthead-->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
            {{-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
        </ol>
        <div class="carousel-inner">
            {{-- <div class="carousel-item active" style="height: 800px">
                <img class="d-block w-100" src="{{ asset('frontend/slider/1.jpeg') }}" style="object-fit: cover"
                    alt="First slide">
                <div class="carousel-caption">
                    <h2 class="animated bounceInRight" style="animation-delay: 1s">We Are Reliable</h2>
                    <h3 class="animated bounceInLeft" style="animation-delay: 2s">Lorem ipsum dolor sit amet.</h3>
                </div>
            </div> --}}
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ asset('frontend/slider/1.png') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/2.png') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/3.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/4.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/5.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/6.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/7.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('frontend/slider/8.jpeg') }}" alt="Second slide">
                <div class="carousel-caption">
                    <h2 class="animated slideInDown" style="animation-delay: 1s">Selamat Datang</h2>
                    <h3 class="animated slideInRight" style="animation-delay: 2s">di Desa Parit Baru</h3>
                </div>
            </div>
            {{-- <div class="carousel-item">
                <img class="d-block w-100" src="..." alt="Third slide">
            </div> --}}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Icons Grid-->
    <section class="showcase">
        <div class="container-fluid p-0">
            <div class="row g-0">
                <div class="col-lg-6 order-lg-2 text-white showcase-img"
                    style="background-image: url('assets/img/bg-showcase-1.jpg')">
                    <div class="container">
                        <h2 class="mb-5">What people are saying...</h2>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6" style="background-color: #f8f9fa;border-radius: 20px">
                                <div class="testimonial-item text-center mx-auto mb-5 mb-lg-0" style="padding: 20px;">
                                    <img class="img-fluid rounded-circle text-center mb-3"
                                        src="https://paritbaru.kaptenku.com/uploads/1655280503profile.jpeg"
                                        alt="..." />
                                    <h5 class="text-center" style="color: rgb(23, 22, 22)">SUHARDI</h5>
                                    <p class="font-weight-light mb-0 text-center" style="color: rgb(23, 22, 22)">
                                        Kepala
                                        Desa</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                    <h2>Sekilas Instansi Desa Parit Baru</h2>
                    <p class="lead mb-0 text-justify">Instansi Desa Parit Baru Kecamatan Selakau Kabupaten Sambas
                        dimana
                        pada saat itu bapak Rajak ditunjuk sebagai kepala desa pertama yang berakhir menjabat pada Tahun
                        1947, bapak Rajak digantikan oleh bapak Bujang M.Noer. Setelah 29 tahun menjabat, pada tahun
                        1976 bapak Bujang M.Noer digantikan oleh bapak Yusuf Jayeng pada tahun 1976 - 1996. Setelah
                        berakhirya masa jabatan bapak Yusuf Jayeng pada Tahun 1996. Terpilihlah bapak Uray M.Tahir
                        menjadi kepala desa sampai pada Tahun 2000. Kemudian pada periode berikutnya, terpilih bapak
                        Haryadi sampai pada Tahun 2001. Berakhirnya masa jabatan bapak Haryadi, pada 2001. Kemudian
                        digantikan bapak Suhardi menjabat sebagai kepala desa parit baru hingga sekarang.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Image Showcases-->

    <!-- Testimonials-->
    <section class="testimonials text-center bg-light">
        <div class="container">
            <h2 class="mb-5">Perangkat Desa</h2>
            <div class="row">
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kadus1.jpeg') }}" alt="..." />
                        <h5>ASNIARDI</h5>
                        <p class="font-weight-light mb-0">Kepala Dusun Pasar Lama</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kadus2.jpeg') }}" alt="..." />
                        <h5>FIRMANSYAH</h5>
                        <p class="font-weight-light mb-0">Kepala Dusun Gaya Baru</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kadus3.jpeg') }}" alt="..." />
                        <h5>YUSNI BANA</h5>
                        <p class="font-weight-light mb-0">Kepala Dusun Siatung</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/sek.jpeg') }}" alt="..." />
                        <h5>AGUS WIRYAWAN</h5>
                        <p class="font-weight-light mb-0">Sekretaris Desa</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kasi1.jpeg') }}" alt="..." />
                        <h5>ARIS FIKRIANSYAH</h5>
                        <p class="font-weight-light mb-0">Kasi Pelayanan</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kasi2.jpeg') }}" alt="..." />
                        <h5>MAHRUS</h5>
                        <p class="font-weight-light mb-0">Kasi Kesejahteraan</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kaurumum.jpeg') }}" alt="..." />
                        <h5>AYU WIDYA</h5>
                        <p class="font-weight-light mb-0">Kaur Umum</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kasi3.jpeg') }}" alt="..." />
                        <h5>DANIL</h5>
                        <p class="font-weight-light mb-0">Kasi Pemerintahan</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kaurkeuangan.jpeg') }}" alt="..." />
                        <h5>GUSTIANI</h5>
                        <p class="font-weight-light mb-0">Kaur Keuangan</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kaurperencanaan.jpeg') }}" alt="..." />
                        <h5>IIS SUGIARTI</h5>
                        <p class="font-weight-light mb-0">Kaur Perencanaan</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/staff.jpeg') }}" alt="..." />
                        <h5>FITRI YANTI</h5>
                        <p class="font-weight-light mb-0">Staff Kesejahteraan</p>
                    </div>
                </div>
                {{-- <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" style="width: 300px;height:200px"
                            src="{{ asset('frontend/kasi2.jpeg') }}" alt="..." />
                        <h5>GUSTIANI</h5>
                        <p class="font-weight-light mb-0">Kaur Keuangan</p>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>


    <div class="modal fade" id="modalPersyaratan" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Persyaratan Pengajuan Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><strong>SURAT KETERANGAN USAHA</strong></p>

                    <p>Persyaratan dokumen yang harus dibawa saat pengambilan pengajuan surat keterangan usaha di kantor
                        desa.</p>

                    <ol>
                        <li>Foto Copy E-KTP 1 Lembar</li>
                    </ol>

                    <p>&nbsp;</p>

                    <p><strong>SURAT KETERANGAN</strong></p>

                    <p>Persyaratan dokumen yang harus dibawa saat pengambilan pengajuan surat keterangan di kantor desa
                        sebagai berikut ;</p>

                    <p><strong>a).&nbsp;</strong>Belum pernah melakukan perekaman -E-KTP<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy KK 1 Lembar</p>

                    <p><strong>b).&nbsp;</strong>Belum meiliki Akta Kelahiran usia 18 tahun<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy KK 1 Lembar<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy E-KTP 1 Lembar<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy Surat Nikah Orang Tua Halaman Foto Pasutri 1 Lembar<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Foto Copy Surat Nikah Orang Tua Halaman Identitas Pasutri 1
                        Lembar</p>

                    <p><strong>c).&nbsp;</strong>Belum memiliki Akta Kelahiran usia dibawah 18 tahun atau masih dibawah
                        &nbsp;umur<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy KK 1 Lembar<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy E-KTP Ayah 1 Lembar<br />
                        &nbsp; &nbsp; &nbsp; - Foto Copy E-KTP Ibu 1 Lembar</p>

                    <p>&nbsp;</p>

                    <p><strong>SURAT KEMATIAN</strong></p>

                    <p>Persyaratan dokumen yang harus dibawa saat pengambilan pengajuan surat kematian di kantor desa.
                    </p>

                    <p>- Foto Copy KK 1 Lembar</p>

                    <p>- Foto Copy Surat Keterangan Kematian Rumah Sakit (jika ada)</p>

                    <p>&nbsp;</p>

                    <p><strong>SURAT KETERANGAN TIDAK MAMPU</strong></p>

                    <p>Persyaratan dokumen yang harus dibawa saat pengambilan pengajuan surat keterangan tidak mampu di
                        kantor desa.</p>

                    <p>- Foto Copy E-KTP 1 Lembar</p>

                    <p>- Foto Copy KK 1 Lembar</p>

                    <br>
                    <center>
                        <img src="{{ asset('frontend/panduan-surat.jpg') }}" style="width: 100%;" alt="">
                    </center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    {{-- <button type="button" class="btn btn-primary">Save</button> --}}
                </div>
            </div>
        </div>
    </div>


    <button type="button" data-toggle="modal" data-target="#modalPersyaratan" class="float">
        Persyaratan Pengajuan Surat
    </button>
    <button type="button" data-toggle="modal" data-target="#modalVideo"
        style="background-color: rgb(136, 209, 233)" class="float2">
        Video Panduan
    </button>
    <!-- Call to Action-->
    {{-- <section class="call-to-action text-white text-center" id="signup">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <h2 class="mb-4">Ready to get started? Sign up now!</h2>
                    <!-- Signup form-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- * * SB Forms Contact Form * *-->
                    <!-- * * * * * * * * * * * * * * *-->
                    <!-- This form is pre-integrated with SB Forms.-->
                    <!-- To make this form functional, sign up at-->
                    <!-- https://startbootstrap.com/solution/contact-forms-->
                    <!-- to get an API token!-->
                    <form class="form-subscribe" id="contactFormFooter" data-sb-form-api-token="API_TOKEN">
                        <!-- Email address input-->
                        <div class="row">
                            <div class="col">
                                <input class="form-control form-control-lg" id="emailAddressBelow" type="email"
                                    placeholder="Email Address" data-sb-validations="required,email" />
                                <div class="invalid-feedback text-white" data-sb-feedback="emailAddressBelow:required">
                                    Email Address is required.</div>
                                <div class="invalid-feedback text-white" data-sb-feedback="emailAddressBelow:email">
                                    Email Address Email is not valid.</div>
                            </div>
                            <div class="col-auto"><button class="btn btn-primary btn-lg disabled"
                                    id="submitButton" type="submit">Submit</button></div>
                        </div>
                        <!-- Submit success message-->
                        <!---->
                        <!-- This is what your users will see when the form-->
                        <!-- has successfully submitted-->
                        <div class="d-none" id="submitSuccessMessage">
                            <div class="text-center mb-3">
                                <div class="fw-bolder">Form submission successful!</div>
                                <p>To activate this form, sign up at</p>
                                <a class="text-white"
                                    href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                            </div>
                        </div>
                        <!-- Submit error message-->
                        <!---->
                        <!-- This is what your users will see when there is-->
                        <!-- an error submitting the form-->
                        <div class="d-none" id="submitErrorMessage">
                            <div class="text-center text-danger mb-3">Error sending message!</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> --}}


    <!-- Button trigger modal -->

    <!-- Modal -->
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Video Panduan Pengajuan Surat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                        <video style="width: 100%" controls id="myVideo">
                            <source src="{{ asset('frontend/video.mp4') }}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                        <p></p>
                        <p class="mb-0"></p>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="stopVideo()" data-dismiss="modal">Tutup</button>
                    {{-- <button type="button" class="btn btn-primary">Save</button> --}}
                </div>
            </div>
        </div>
    </div>

    <!-- Footer-->
    <footer class="footer bg-light" style="background-color: #4F5551 !important">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 h-100 text-center text-lg-start my-auto">
                    <div class="col-md-12">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{ asset('frontend/logo.jpg') }}" width="200" alt="">
                        </a>
                    </div>
                    <br>
                    <ul class="list-inline mb-2" style="color: white">
                        <li class="list-inline-item"><a href="{{ route('login') }}">Login</a></li>
                        <li class="list-inline-item"><a href="{{ route('registrasi') }}">Registrasi</a></li>
                    </ul>
                    <p class="text-muted small mb-4 mb-lg-0" style="color: white !important">&copy;2022 Sistem E-surat
                        Pemerintah Desa Parit Baru.</p>
                </div>
                <div class="col-lg-6 h-100 col-md-6 col-sm-12 text-center text-lg-end my-auto" id="box-map">
                    <div class="mapouter">
                        <div class="gmap_canvas"><iframe width="600" height="200" id="gmap_canvas"
                                src="https://www.google.com/maps/embed?pb=!1m19!1m8!1m3!1d8770.748350342876!2d108.96824851325029!3d1.0617633548633754!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x31e37b6f9f206d31%3A0xba2c005900c5dabc!2smap%20kantor%20desa%20parit%20Baru%20kecamatan%20selakau%20kabupaten%20sambas!3m2!1d1.0615614!2d108.97266839999999!5e1!3m2!1sid!2sid!4v1650891965333!5m2!1sid!2sid"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a
                                href="https://fmovies-online.net">fmovies</a><br>
                            <style>
                                .mapouter {
                                    position: relative;
                                    text-align: right;
                                    height: 200px;
                                    width: 600px;
                                }
                            </style><a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
                            <style>
                                .gmap_canvas {
                                    overflow: hidden;
                                    background: none !important;
                                    height: 200px;
                                    width: 600px;
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>

    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    
    <script>
     var video = document.getElementById("myVideo");
        function stopVideo(){
           
            video.pause();
        }
    </script>
</body>

</html>
