<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="ti-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('submissions.add') }}" class="waves-effect">
                        <i class="ti-email"></i>
                        <span>Pengajuan Surat</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('submissions.index') }}" class="waves-effect">
                        <i class="ti-reload"></i>
                        <span>Riwayat Pengajuan</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('account') }}" class="waves-effect">
                        <i class="mdi mdi-clipboard-list-outline"></i>
                        <span>Data Diri</span>
                    </a>
                </li>
                {{-- <li>
                    <a href="https://aguswahyu.com/panduan-esurat-warga/" target="_blank" class="waves-effect">
                        <i class="mdi mdi-help"></i>
                        <span>Panduan</span>
                    </a>
                </li> --}}
                @if (Auth::user()->is_signatories == 1)
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="ti-email"></i>
                            <span>Layanan Surat Masuk</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="{{ route('submissions.approved') }}">Disetujui</a></li>
                            <li><a href="{{ route('submissions.selesai') }}">Selesai</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="ti-folder"></i>
                            <span>Arsip Surat</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li>
                                <a href="#" class="has-arrow waves-effect">
                                    <i class="ti-download"></i>
                                    <span>Surat Masuk Pemerintahan</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">

                                    {{-- <a href="{{ route('arsip', 'umum') }}" class="waves-effect">Gobi Surat
                                        Masuk Umum</a> --}}
                                    <a href="{{ route('arsip', 'pemerintahan') }}" class="waves-effect">Gobi
                                        Surat Masuk Pemerintahan</a>

                                </ul>
                            </li>
                            <li>
                                <a href="#" class="has-arrow waves-effect">
                                    <i class="ti-upload"></i>
                                    <span>Surat Keluar Masyarakat</span>
                                </a>
                                <ul class="sub-menu" aria-expanded="false">
                                    @foreach (lettersAll() as $item)
                                        @if ($item->id == 4)
                                            <a href="{{ route('arsip.out.q', 1) }}" class="waves-effect">Gobi
                                                Surat Keterangan E-KTP</a>
                                            <a href="{{ route('arsip.out.q', 2) }}" class="waves-effect">Gobi
                                                Surat Keterangan Akta Kelahiran</a>
                                        @endif
                                        @if ($item->id != 4)
                                            <a href="{{ route('arsip.out', $item->id) }}" class="waves-effect">Gobi
                                                {{ $item->name }}</a>
                                        @endif
                                    @endforeach
                                    <a href="{{ route('arsip', 'keluar-pemerintahan') }}" class="waves-effect">Gobi
                                        Surat
                                        Keluar Pemerintahan</a>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="ti-files"></i>
                            <span>Laporan</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li>
                                @foreach (lettersAll() as $item)
                                    @if ($item->id == 4)
                                        <a href="{{ route('laporan.q', 1) }}" class=" waves-effect">
                                            <span>Laporan Surat Keterangan E-KTP</span>
                                        </a>
                                        <a href="{{ route('laporan.q', 2) }}" class=" waves-effect">
                                            <span>Laporan Surat Keterangan Akta Kelahiran</span>
                                        </a>
                                    @endif
                                    @if ($item->id != 4)
                                        <a href="{{ route('laporan', $item->id) }}" class=" waves-effect">
                                            <span>Laporan {{ $item->name }}</span>
                                        </a>
                                    @endif
                                @endforeach
                                <a href="{{ route('laporan.masuk', 'pemerintahan') }}" class=" waves-effect">
                                    <span>Laporan Surat Masuk Pemerintahan</span>
                                </a>
                                <a href="{{ route('laporan.masuk', 'keluar-pemerintahan') }}" class=" waves-effect">
                                    <span>Laporan Surat Keluar Pemerintahan</span>
                                </a>

                            </li>

                        </ul>
                    </li>
                @endif

                <li>
                    <a href="javascript:void(0);"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                        class="waves-effect">
                        <i class="mdi mdi-logout"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
