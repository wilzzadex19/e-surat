<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PasswordRequest;
use App\User;
use App\ActivityLog;
use Activity;
use Hash;
use Image;
use Illuminate\Support\Facades\Hash as FacadesHash;

class AccountController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return view('account.index', ['user' => $user]);
    }

    public function indexEdit()
    {
        $user = auth()->user();
        $data['user'] = $user;
        return view('account.edit', $data);
    }

    public function update(Request $request)
    {
        $cek = User::where('sin', $request->sin)->where('id', '!=', auth()->user()->id)->count();

        if ($cek > 0) {
            return redirect()->back()->with(
                'error',
                'Registrasi gagal, Nik ' . $request->sin . ' telah terdaftar',
            );
        }
        $cek_email = User::where('email', $request->email)->where('id', '!=', auth()->user()->id)->count();

        if ($cek_email > 0) {
            return redirect()->back()->with(
                'error',
                'Registrasi gagal, Email ' . $request->email . ' telah terdaftar',
            );
        }


        $user = User::findOrFail(auth()->user()->id);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $path = 'uploads/';

            if (!file_exists(public_path($path))) {
                mkdir($path, 666, true);
            }

            $path .= time() . 'edit.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 300);
            $image->save(public_path($path));
            $user->photo = $path;
        }

        $user->sin = $request->sin;
        $user->name = $request->name;

        // $password = $request->password;
        // $user->password = FacadesHash::make($password);

        $user->birth_place = $request->birth_place;
        $user->birth_date = $request->birth_date;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->religion = $request->religion;
        $user->marital_status = $request->marital_status;
        $user->profession = $request->profession;
        $user->phone_number = $request->phone_number;
        $user->is_active = 1;
        $user->save();

        $nik = $request->sin;
        $nik_unik = substr($nik, 0, 6);
        // if ($nik_unik != '610107') {
        //     $user = User::find($user->id);
        //     $user->is_active = 2;
        //     $user->save();

        //     $messages = 'Mohon maaf akun anda tidak diaktifkan karena terdeteksi bukan merupakan warga parit baru';
        //     $data['user'] = $user;
        //     $data['messages'] = $messages;
        //     $email = [$user->email];
        //     // return view('email.user_approval',$data);

        //     Mail::send('email.user_approval', $data, function ($message) use ($email) {
        //         $message->to($email)->subject('Registrasi Berhasil');
        //     });

        //     return redirect()->route('registrasi')->with(
        //         'error',
        //         'Registrasi gagal, akun anda tidak diaktifkan karena terdeteksi bukan merupakan warga parit baru',
        //     );
        // }

        // Validasi nik

        $messages = 'Selamat, anda berhasil melakukan registrasi';
        $data['user'] = $user;
        $data['messages'] = $messages;
        $email = [$user->email];
        // return view('email.user_approval',$data);

        // Mail::send('email.user_approval', $data, function ($message) use ($email) {
        //     $message->to($email)->subject('Akun ditolak');
        // });

        Activity::add(['page' => 'Registrasi', 'description' => 'Melakukan Registrasi: ' . $request->name]);

        return redirect()->back()->with(
            'success',
            'Selamat, berhasil menyimpan perubahan',
        );
    }


    public function whatsapp(Request $request)
    {
        $data = auth()->user();
        $data->phone_number = $request->phone_number;
        $data->save();

        Activity::add(['page' => 'Account', 'description' => 'Anda Mengganti Nomor Whatsapp']);

        return back()->with([
            'status' => 'success',
            'message' => 'Nomor Whatsapp Berhasil Disimpan!'
        ]);
    }

    public function password()
    {
        return view('account.password');
    }

    public function patchPassword(PasswordRequest $request)
    {
        $data = auth()->user();
        $data->password = Hash::make($request->new_password);
        $data->save();

        Activity::add(['page' => 'Ganti Password', 'description' => 'Anda Mengganti Password']);

        return back()->with([
            'status' => 'success',
            'message' => 'Password Berhasil Diubah!'
        ]);
    }

    public function logs()
    {

        $logs = ActivityLog::orderBy('created_at', 'desc')
            ->where('user_type', 'user')
            ->where('user_id', auth()->user()->id)
            ->get();

        return view('account.logs', ['logs' => $logs]);
    }
}
