<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Letter;
use App\Submission;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // dd($data);

        if (Auth::user()->is_signatories == 1) {
            $letters = Letter::all();
            $data['submissions'] = Submission::count();
            $data['submissionsApproved'] = Submission::where('approval_status', 1)->count();
            $data['submissionSelesai'] = Submission::where('approval_status',3)->count();
            $data['submissionDitolak'] = Submission::where('approval_status',2)->count();
            $data['users'] = User::where('is_signatories','!=',1)->count();
            $data['letters'] = Letter::count();
            return view('home', $data);
        } else {
            $letters = Letter::all();
            $data['submissions'] = Submission::where('user_id', auth()->user()->id)->count();
            $data['submissionsApproved'] = Submission::where('user_id', auth()->user()->id)->where('approval_status', 1)->count();
            $data['submissionsPending'] = Submission::where('user_id', auth()->user()->id)->where('approval_status', 0)->count();
            $data['submissionsSelesai'] = Submission::where('user_id', auth()->user()->id)->where('approval_status', 3)->count();
            $data['submissionsDitolak'] = Submission::where('user_id', auth()->user()->id)->where('approval_status', 2)->count();
            $data['letters'] = $letters;
            return view('home', $data);
        }


      
    }

    public function index2()
    {

        return view('fe.index');
    }
}
