<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Activity;
use App\Http\Controllers\Controller;
use App\Letter;
use App\Submission;
use App\User;
use Illuminate\Http\Request;

class FormSuratController extends Controller
{
    public function index(){
        $warga = User::where('is_signatories',null)->orderBy('name','desc')->get();
        $letters = Letter::all();
        $data['letters'] = $letters;
        $data['warga'] = $warga;
        return view('admin.form_surat.index',$data);
    }

    public function store($letter, Request $request)
    {
        // dd($request->all());
        $letter = Letter::find($letter);
        // dd($letter);

        $awalan = $letter->number_format . '/';
        $query = Submission::select('number')->where('letter_id', $letter->id)->orderBy('number', 'desc');

        $lebar = 1;
        $jumlahrecord = $query->count();

        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]['number'], strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;


        $nomor_surat = $angka . '/Pem' . '/' . date('Y');





        if (!$letter) {
            return back()->with([
                'status' => 'danger',
                'message' => 'Surat Belum Tersedia!'
            ]);
        }

        $except = ['_token'];

        $submission = new Submission;
        $submission->user_id = $request->user_id;
        $submission->number = $nomor_surat;
        $submission->letter_id = $letter->id;
        $submission->generate_by_admin = 1;
        $submission->kepentingan = $request->kepentingan_surat;
        $submission->approval_status = '1';
        $submission->approval_at = now();
        $submission->admin_id = auth('admin')->user()->id;
        foreach ($request->all() as $key => $valData) {
            if ($request->hasFile($key)) {
                $except[] .= $key;
                $file = $request->file($key);
                $path = 'submission_data/';


                if (!file_exists(public_path($path))) {
                    mkdir($path, 777, true);
                }

                $file_name = $key . '-' . str_replace('/', '-', $nomor_surat)  . '.' . $file->getClientOriginalExtension();
                $fullPath = $path  . $file_name;
                $file->move($path, $file_name);
                $request->request->add([$key. 's' => $fullPath]);
            }
        }
        $submission->data = json_encode($request->except($except));
        
        if($letter->id == 4){
            $submission->is_under_age = $request->usia_syarat;
            $submission->keterangan_pengajuan = $request->keterangan_pengajuan;
        }

        // dd($submission);
        $submission->save();

        // $response = Http::withHeaders(['Authorization' => config('whatsapp.token')])
        // ->asForm()
        // ->post('https://fonnte.com/api/send_message.php', [
        //     'phone' => setting('whatsapp'),
        //     'type' => 'text',
        //     'text' => 'Ada Pengajuan Surat Baru Oleh: ' . auth()->user()->name . ' Dan Surat Yang Diajukan Adalah: ' . $letter->name . '. Mohon Segera Ditinjau, Terimakasih. E-Surat Pemerintah Desa fn s'
        // ]);

        Activity::add(['page' => 'Pengajuan Surat', 'description' => 'Mengajukan Surat Baru: ' . $letter->name]);

        return back()->with([
            'status' => 'success',
            'message' => 'Berhasil Membuat Surat! Silahkan cetak di bagian menu Disetujui'
        ]);
    }
}
