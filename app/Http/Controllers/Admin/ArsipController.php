<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Letter;
use App\Letter_Archieve;
use App\Submission;
use Illuminate\Http\Request;

class ArsipController extends Controller
{
    public function index($cat)
    {
        $arsip = Letter_Archieve::where('letter_category', $cat)->where('is_surat_keluar_pemerintahan',null)->where('submission_id', null)->get();
        if($cat == 'keluar-pemerintahan'){
            $arsip = Letter_Archieve::where('is_surat_keluar_pemerintahan',1)->where('submission_id', null)->get();
        }
        $data['arsip'] = $arsip;
        $data['label'] = ucfirst($cat);

        if($cat == 'keluar-pemerintahan'){
            return view('admin.arsip.keluar_pemerintahan', $data);
        }
        // dd($cat);
        return view('admin.arsip.masuk', $data);
    }

    public function detail($id)
    {
        $arsip = Letter_Archieve::findOrFail($id);
        $data['arsip'] = $arsip;

        if($arsip->is_surat_keluar_pemerintahan == 1){
            return view('admin.arsip.keluar_show_pemerintahan', $data);
        }

        // dd($data);
        return view('admin.arsip.masuk_show', $data);
    }

    public function detailOut($id)
    {
        $arsip = Letter_Archieve::findOrFail($id);
        $data['arsip'] = $arsip;
        return view('admin.arsip.keluar_show', $data);
    }

    public function indexOut($letter_id)
    {
        $arsip = Letter_Archieve::where('letter_id', $letter_id)->get();
        $letter = Letter::find($letter_id);
        $data['arsip'] = $arsip;
        $data['letter'] = $letter;
        return view('admin.arsip.keluar', $data);
    }
    public function indexOutQ($letter_id)
    {
        $arsip = Letter_Archieve::where('letter_id', 4)->whereHas('submission', function ($q) use ($letter_id) {
            $q->where('keterangan_pengajuan', $letter_id);
        })->get();
        $letter = Letter::find(4);
        $data['arsip'] = $arsip;
        $data['letter'] = $letter;
        $data['k'] = $letter_id;
        return view('admin.arsip.keluar', $data);
    }
}
