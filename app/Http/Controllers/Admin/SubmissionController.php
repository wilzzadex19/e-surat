<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Submission;
use App\Signatory;
use App\Signatories;
use App\Exports\SubmissionsExport;
use Activity;
use App\Letter;
use Excel;
use Illuminate\Support\Facades\Mail;
use PhpParser\Lexer;

class SubmissionController extends Controller
{
    public function pending()
    {
        $submissions = Submission::with('user', 'letter')
            ->where('approval_status', 0)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.submission.pending', ['submissions' => $submissions]);
    }

    public function approved()
    {
        $submissions = Submission::with('user', 'letter', 'admin')
            ->where('approval_status', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.submission.approved', ['submissions' => $submissions]);
    }

    public function exportApproved()
    {
        return Excel::download(new SubmissionsExport, 'Surat Yang Disetujui ' . now()->format('d-M-Y') . '.xlsx');
    }

    public function rejected()
    {
        $submissions = Submission::with('user', 'letter', 'admin')
            ->where('approval_status', 2)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.submission.rejected', ['submissions' => $submissions]);
    }
    public function selesai()
    {
        $submissions = Submission::with('user', 'letter', 'admin')
            ->where('approval_status', 3)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.submission.selesai', ['submissions' => $submissions]);
    }

    public function show($id)
    {
       
        $submission = Submission::find($id);
        $signatories = Signatory::all();
        $letter = Letter::findOrFail($submission->letter_id);
        $awalan = $letter->number_format . '/';
        $query = Submission::select('number')->where('number','!=',null)->where('letter_id', $letter->id)->orderBy('number', 'desc');

        $lebar = 1;
        $jumlahrecord = $query->count();

        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]['number'], strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;
        $nomor_surat = $angka . '/PEM' . '/' . date('Y');
        // $data['no_surat'] = $nomor_surat;
        return view('admin.submission.show', ['submission' => $submission, 'signatories' => $signatories,'no_surat' => $nomor_surat]);
    }

    public function status($id, $status, Request $request)
    {
        $submission = Submission::find($id);
        $submission->approval_status = $status;
        $submission->approval_at = now();
        $submission->admin_id = 1;
        if ($status == 2) {
            $submission->notes = $request->notes;
        }


        if ($status == 1) {
            $letter = $submission->letter;
            $awalan = $letter->number_format . '/';
            $query = Submission::select('number')->where('letter_id', $letter->id)->orderBy('number', 'desc');

            $lebar = 1;
            $jumlahrecord = $query->count();

            if ($jumlahrecord == 0) {
                $nomor = 1;
            } else {
                $row = $query->get()->toArray();
                $nomor = intval(substr($row[0]['number'], strlen($awalan))) + 1;
            }

            if ($lebar > 0)
                $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
            else
                $angka = $awalan . $nomor;




            $nomor_surat = $angka . '/PEM' . '/'.'IX/' . date('Y');

            $submission->number =  $nomor_surat;
            $messages = 'Pengajuan surat "' . $submission->letter->name . '" Bernomor ' . $submission->number . ' oleh: ' . $submission->user->name . ' telah disetujui. Silakan datang ke Kantor Desa dengan Menggunakan Masker!';
            $redirect_route = route('admin.submissions.approved');
            $subject = 'Surat Disetujui';
        } elseif ($status == 3) {
            $messages = 'Pengajuan surat "' . $submission->letter->name . '" Bernomor ' . $submission->number . ' oleh: ' . $submission->user->name . ' telah ditandatangani dan dinyatakan selesai.';
            $redirect_route = route('submissions.selesai');
            $subject = 'Surat Selesai';
        } else {
            $messages = 'Pengajuan "' . $submission->letter->name . '" Bernomor ' . $submission->number . ' oleh: ' . $submission->user->name . ' telah ditolak!';
            $redirect_route = route('admin.submissions.rejected');
            $subject = 'Surat Ditolak';
        }
        $submission->save();

        $data['submission'] = $submission;
        $data['messages'] = $messages;
        $email = [$submission->user->email];
        // return view('email.submission_approval',$data);

        Mail::send('email.submission_approval', $data, function ($message) use ($email, $subject) {
            $message->to($email)->subject($subject);
        });

        // $response = Http::withHeaders(['Authorization' => config('whatsapp.token')])
        // ->asForm()
        // ->post('https://fonnte.com/api/send_message.php', [
        //     'phone' => $submission->user->phone_number,
        //     'type' => 'text',
        //     'text' => $message
        // ]);

        Activity::add(['page' => 'Warga', 'description' => 'Berhasil Mengubah Status Pengajuan Surat: #' . $id]);

        // $route = $status == 1 ?  : route('admin.submissions.rejected');
        return redirect($redirect_route)->with([
            'status' => 'success',
            'message' => 'Mengubah Status Pengajuan Surat: #' . $id
        ]);
    }

    public function update(Request $request, $id)
    {
        $submission = Submission::find($id);

        $data_old = json_decode($submission->data,true);

        // dd($data_old);
        
        $file_old = [];
        foreach($data_old as $key => $do){
            if (strpos($key, 'file') !== false || strpos($key, 'foto') !== false) {
                $file_old[$key] = $do;
            }
        }

        $new_data = $request->except('_token', '_method', 'number');
        $arr_merge = array_merge($new_data,$file_old);

        $submission->number = $request->number;
        $submission->data = json_encode($arr_merge);
        $submission->save();

        Activity::add(['page' => 'Kaur Umum', 'description' => 'Berhasil Memperbarui Pengajuan Surat: #' . $id]);

        return back()->with([
            'status' => 'success',
            'message' => 'Memperbarui Pengajuan Surat: #' . $id
        ]);
    }

    public function print($id, Request $request)
    {
        $submission = Submission::find($id);

        $signatory_id = $request->signatory ?? setting('signatory_active');
        $signatory = Signatory::where('is_active', 1)->first();

        $data['signatory_name'] = $signatory->name;
        $data['signatory_position'] = $signatory->position;
        $data['on_behalf'] = $signatory_id != 1 ? 'A/N, Perbengkel ' . ucwords(strtolower(setting('village'))) : '';

        foreach ($submission->user->toArray() as $key => $value) {
            $data[$key] = $value;
        }

        $data['ttl'] = $submission->user->getPsb();
        $data['tgl'] = now()->formatLocalized('%d %B %Y');
        $data['districts'] = ucwords(strtolower(setting('districts')));
        $data['sub-districts'] = ucwords(strtolower(setting('sub-districts')));
        $data['village'] = ucwords(strtolower(setting('village')));

        foreach (json_decode($submission->data) as $key => $value) {
            $data[$key] = $value;
        }

        $content = $submission->letter->content;
        foreach ($data as $key => $value) {
            $content = str_replace('[' . $key . ']', $value, $content);
            if($submission->letter_id == 4){
                $fix = $submission->keterangan_pengajuan == 1 ? 'Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP' : 'Belum Memiliki Akta Kelahiran' ;
                 $content = str_replace('[keterangan_pengajuan]', $fix, $content);
            }
        }

        return view('admin.print.template', ['submission' => $submission, 'content' => $content]);
    }
}
