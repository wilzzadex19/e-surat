<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Activity;
use App\Http\Controllers\Controller;
use App\Letter_Archieve;
use App\Submission;
use Illuminate\Http\Request;

class PencatatanSuratController extends Controller
{
    public function index()
    {
        $submissions = [];
        $archi = Letter_Archieve::where('letter_type', 'surat-keluar')->get();
        foreach($archi as $val){
            if($val->submission_id != null){
                $submissions[] .= $val->submission_id;
            }
        }
       
        $data['submission'] = Submission::where('approval_status', 3)->whereNotIn('id',$submissions)->orderBy('number', 'desc')->get();
        return view('admin.pencatatan.index', $data);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $new = new Letter_Archieve();
        $new->letter_type = $request->jenis_surat;
        $new->letter_category = $request->kategori_surat;
        $new->letter_trait = $request->sifat_surat;



        if ($request->jenis_surat == 'surat-masuk') {
            $f_name = $request->no_surat;
            $new->letter_date_receive = $request->tgl_surat_diterima;
            $new->letter_number = $request->no_surat;
            $new->regarding = $request->perihal;
            $new->receive_by = $request->diterima_oleh;
            $new->from = $request->asal_surat;
        } else {


            if ($request->kategori_surat == 'pemerintahan') {
                $new->is_surat_keluar_pemerintahan = 1;
                $new->submission_id = $request->letter_id;
                $new->to = $request->tujuan_surat;
                $new->letter_date_out = $request->tgl_surat_dikeluarkan;
                $new->out_by = $request->dikeluarkan_oleh;
                $new->letter_number = $request->no_surat;
            } else {
                $sub = Submission::find($request->letter_id);
                $new->submission_id = $request->letter_id;
                $new->letter_id = $sub->letter_id;
                $new->to = $request->tujuan_surat;
                $new->letter_date_out = $request->tgl_surat_dikeluarkan;
                $new->out_by = $request->dikeluarkan_oleh;
            }
        }

        if ($request->hasFile('lampiran')) {
            $file = $request->file('lampiran');
            $path = 'arsip_file/';


            if (!file_exists(public_path($path))) {
                mkdir($path, 777, true);
            }

            $file_name = 'lampiran' . '-' . time()  . '.' . $file->getClientOriginalExtension();
            $fullPath = $path  . $file_name;
            $file->move($path, $file_name);
            $new->file = $fullPath;
        }

        // dd($new);

        $new->save();

        Activity::add(['page' => 'Pencatatan Surat', 'description' => 'Menambah data']);

        return redirect()->route('admin.pencatatan')->with([
            'status' => 'success',
            'message' => 'Surat berhasil di arsipkan',
        ]);
    }
}
