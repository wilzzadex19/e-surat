<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Letter;
use App\Letter_Archieve;
use App\Submission;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index($letter_id)
    {
        $submission = Submission::where('letter_id', $letter_id)->orderBy('number', 'asc')->get();
        $letter = Letter::find($letter_id);
        $data['letter'] = $letter;
        $data['submissions'] = $submission;
        return view('admin.laporan.index', $data);
    }
    public function indexQ($letter_id)
    {
        $submission = Submission::where('letter_id', 4)->where('keterangan_pengajuan', $letter_id)->orderBy('number', 'asc')->get();
        $letter = Letter::find(4);
        $data['letter'] = $letter;
        $data['submissions'] = $submission;
        $data['k'] = $letter_id;
        return view('admin.laporan.index', $data);
    }

    public function indexMasuk($jenis)
    {
        $arsip = Letter_Archieve::where('letter_category', $jenis)->where('is_surat_keluar_pemerintahan', null)->where('letter_id', null)->get();
        if ($jenis == 'keluar-pemerintahan') {
            $arsip = Letter_Archieve::where('is_surat_keluar_pemerintahan', 1)->where('letter_id', null)->get();
        }
        // dd($arsip);
        $data['arsip'] = $arsip;
        $data['label'] = ucfirst($jenis);
        $data['jenis'] = $jenis;


        if ($jenis == 'keluar-pemerintahan') {
            return view('admin.laporan.keluar_pemerintahan', $data);
        }
        return view('admin.laporan.masuk', $data);
    }

    public function cetakMasuk(Request $request)
    {
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0] . ' 00:00:00';
        $tanggal_akhir = $pisah[1] . ' 23:59:00';
        $jenis = $request->jenis;
        $arsip = Letter_Archieve::where('letter_category', $jenis)->where('is_surat_keluar_pemerintahan', null)->where('letter_id', null)->whereBetween('created_at',[$tanggal_awal,$tanggal_akhir])->get();
        if ($jenis == 'keluar-pemerintahan') {
            $arsip = Letter_Archieve::where('is_surat_keluar_pemerintahan', 1)->where('letter_id', null)->whereBetween('created_at',[$tanggal_awal,$tanggal_akhir])->get();
        }
        // dd($arsip);
        $data['arsip'] = $arsip;
        $data['label'] = ucfirst($jenis);
        $data['jenis'] = $jenis;
        $data['judul'] = 'Laporan Surat Masuk ' . $data['label'] . ' <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
        return view('admin.laporan.cetak_masuk_pemerintahan', $data);
    }

    public function cetak(Request $request)
    {
        // dd($request->all());
        if ($request->letter_id != 4) {
            $pisah = explode('/', $request->tanggal);
            $tanggal_awal = $pisah[0] . ' 00:00:00';
            $tanggal_akhir = $pisah[1] . ' 23:59:00';

            $submission = Submission::where('letter_id', $request->letter_id)->whereBetween('created_at', [$tanggal_awal, $tanggal_akhir])->orderBy('number', 'asc')->get();
            $letter = Letter::find($request->letter_id);
            $data['letter'] = $letter;
            $data['submissions'] = $submission;
            $data['judul'] = 'Laporan ' . $letter->name . ' <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
            // dd($data);



            return view('admin.laporan.cetak', $data);
        } else {
            // dd($request->all());
            $pisah = explode('/', $request->tanggal);
            $tanggal_awal = $pisah[0] . ' 00:00:00';
            $tanggal_akhir = $pisah[1] . ' 23:59:00';

            $submission = Submission::where('letter_id', 4)->where('keterangan_pengajuan', $request->k)->whereBetween('created_at', [$tanggal_awal, $tanggal_akhir])->orderBy('number', 'asc')->get();
            $letter = Letter::find($request->letter_id);
            $data['letter'] = $letter;
            $data['submissions'] = $submission;
            $data['judul'] = 'Laporan Surat Keterangan (' . ($request->k == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)') . ') <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
            // dd($data);



            return view('admin.laporan.cetak', $data);
        }
    }

    public function cetakPemerintahan(Request $request)
    {
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0] . ' 00:00:00';
        $tanggal_akhir = $pisah[1] . ' 23:59:00';
        $submission = Letter_Archieve::where('is_surat_keluar_pemerintahan', 1)->whereBetween('created_at', [$tanggal_awal, $tanggal_akhir])->orderBy('letter_number', 'asc')->get();
        $data['submissions'] = $submission;
        $data['judul'] = 'Laporan Surat Keluar Pemerintahan <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
        // dd($data);



        return view('admin.laporan.cetak_p', $data);
    }
}
