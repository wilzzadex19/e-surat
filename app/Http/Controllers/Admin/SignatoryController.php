<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Activity;
use App\Http\Controllers\Controller;
use App\Signatory;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\SignatoryRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class SignatoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.signatory.index', ['signatories' => Signatory::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.signatory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SignatoryRequest $request)
    {
        // dd($request->all());
        $signatory = new Signatory();
        $signatory->name = $request->name;
        $signatory->position = $request->position;
        $signatory->is_active = $request->status;


        if ($signatory->save()) {
            $user = new User();

            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $path = 'uploads/';

                if (!file_exists(public_path($path))) {
                    mkdir($path, 777, true);
                }

                $path .= time() . '.' . $file->getClientOriginalExtension();
                $image = Image::make($file)->resize(300, 300);
                $image->save(public_path($path));
                $user->photo = $path;
            }

            $user->sin = $request->sin;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make('kepaladesa2022');
            $user->is_signatories = 1;
            $user->is_active = 1;
            $user->save();

            $update_user_id = Signatory::where('id',$signatory->id)->update([
                'user_id' => $user->id,
            ]);

        }

        if ($request->status == 1) {
            $updtes = Signatory::where('is_active', 1)->where('id', '!=', $signatory->id)->update([
                'is_active' => 0
            ]);
        }


        Activity::add(['page' => 'Kepala Desa', 'description' => 'Menambah Data Baru: ' . $request->name]);

        return redirect()->route('admin.signatories.index')->with([
            'status' => 'success',
            'message' => 'Menambahkan Data Baru: ' . $request->name
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Signatory  $signatory
     * @return \Illuminate\Http\Response
     */
    public function show(Signatory $signatory)
    {
        return view('admin.signatory.show', ['signatory' => $signatory]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Signatory  $signatory
     * @return \Illuminate\Http\Response
     */
    public function edit(Signatory $signatory)
    {
        return view('admin.signatory.edit', ['signatory' => $signatory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Signatory  $signatory
     * @return \Illuminate\Http\Response
     */
    public function update(SignatoryRequest $request, Signatory $signatory)
    {
        $signatory->find($signatory->id);
        $signatory->name = $request->name;
        $signatory->position = $request->position;
        $signatory->is_active = $request->status;
        $signatory->save();

        if ($request->status == 1) {
            $updtes = Signatory::where('is_active', 1)->where('id', '!=', $signatory->id)->update([
                'is_active' => 0
            ]);
        }

        Activity::add(['page' => 'Edit Penandatangan', 'description' => 'Memperbarui Kepala Desa: ' . $signatory->name]);

        return redirect()->route('admin.signatories.index')->with([
            'status' => 'success',
            'message' => 'Berhasil Memperbarui Kepala Desa: ' . $signatory->name
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Signatory  $signatory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Signatory $signatory)
    {
        
        Activity::add(['page' => 'Kepala Desa', 'description' => 'Menghapus Kepala Desa: ' . $signatory->name]);
        User::where('id',$signatory->user_id)->delete();
        $signatory->delete();

        return back()->with(['status' => 'success', 'message' => 'Kepala Desa Berhasil Dihapus!']);
    }
}
