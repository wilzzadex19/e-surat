<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\PasswordRequest;
use App\Admin;
use App\ActivityLog;
use Activity;
use Hash;
use Image;

class AccountController extends Controller
{
    public function index()
    {
        $admin = auth('admin')->user();
        return view('admin.account.index', ['admin' => $admin]);
    }

    public function update(Request $request)
    {
        $user = Admin::find(auth('admin')->user()->id);

        $cek = Admin::where('username', $request->username)->where('id', '!=', $user->id)->count();
        if ($cek > 0) {
            return back()->with([
                'status' => 'error',
                'message' => 'Username telah terdaftar!'
            ]);
        }

        $user->username = $request->username;
        $user->name = $request->name;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $path = 'uploads/';

            if (!file_exists(public_path($path))) {
                mkdir($path, 666, true);
            }

            $path .= time() . 'profile.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 300);
            $image->save(public_path($path));
            $user->photo = $path;
        }
        $user->save();

        // $user->update($request->validated());

        Activity::add(['page' => 'Data Akun', 'description' => 'Memperbarui Data Akun']);

        return back()->with([
            'status' => 'success',
            'message' => 'Akun Berhasil Diperbarui!'
        ]);
    }

    public function password()
    {
        return view('admin.account.password');
    }

    public function patchPassword(PasswordRequest $request)
    {
        $data = Admin::find(auth('admin')->user()->id);
        $data->password = Hash::make($request->new_password);
        $data->save();

        Activity::add(['page' => 'Ganti Password', 'description' => 'Anda Mengganti Password']);

        return back()->with([
            'status' => 'success',
            'message' => 'Password Berhasil Diubah!'
        ]);
    }

    public function logs()
    {
        $logs = ActivityLog::orderBy('created_at', 'desc')
            ->where('user_type', 'admin')
            ->where('user_id', auth('admin')->user()->id)
            ->get();

        return view('admin.account.logs', ['logs' => $logs]);
    }
}
