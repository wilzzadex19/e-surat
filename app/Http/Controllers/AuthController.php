<?php

namespace App\Http\Controllers;

use App\Helpers\Activity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Auth;
use App\User;
use Image;
use Hash;
use Illuminate\Support\Facades\Hash as FacadesHash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use ThrottlesLogins;

    public function username()
    {
        return 'sin';
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validator($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if (Auth::attempt($request->only($this->username(), 'password'), $request->filled('remember'))) {
            // dd(Auth::user());
            if (Auth::user()->is_active == 0) {
                Auth::logout();
                return redirect()->route('login')->with('gagal', 'Akun anda belum di setujui oleh admin');
            } else if (Auth::user()->is_active == 2) {
                Auth::logout();
                return redirect()->route('login')->with('gagal', 'Akun anda ditolak oleh admin dikarenakan bukan merupakan warga Desa Parit Baru');
            }

            Activity::add(['page' => 'Login', 'description' => 'Masuk Ke Website']);

            return redirect()->route('home')->with('status', 'You are Logged!');
        }

        $this->incrementLoginAttempts($request);

        return $this->loginFailed();
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('status', 'You has been logged out!');
    }

    private function validator(Request $request)
    {
        $rules = [
            $this->username() => 'required|string|exists:users|min:4|max:191',
            'password'        => 'required|string|min:6|max:255',
        ];

        $messages = [
            $this->username() . '.exists' => 'These credentials do not match our records.',
        ];

        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        return redirect()->back()->withInput()->withErrors([
            'password' => 'Wrong password!',
        ]);
    }

    public function registrasi()
    {
        return view('auth.registrasi');
    }

    public function registrasiStore(Request $request)
    {

        $cek = User::where('sin', $request->sin)->count();

        if ($cek > 0) {
            $arrError['message'] =  'Registrasi gagal, Nik ' . $request->sin . ' telah terdaftar';
            $arrError['type'] = 'nik';
            $arrError['req'] = $request->except('photo');
            return redirect()->back()->with(
                'error',
                $arrError,
            );
        }
        $cek_email = User::where('email', $request->email)->count();

        if ($cek_email > 0) {
            $arrError['message'] =  'Registrasi gagal, Email ' . $request->email . ' telah terdaftar';
            $arrError['type'] = 'email';
            $arrError['req'] = $request->except('photo');
            return redirect()->back()->with(
                'error',
                $arrError,
            );
        }


        $user = new User();

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $path = 'uploads/';

            if (!file_exists(public_path($path))) {
                mkdir($path, 666, true);
            }

            $path .= time() . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(300, 300);
            $image->save(public_path($path));
            $user->photo = $path;
        }

        $user->sin = $request->sin;
        $user->name = $request->name;

        $password = $request->password;
        $user->password = FacadesHash::make($password);

        $user->birth_place = $request->birth_place;
        $user->birth_date = $request->birth_date;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->religion = $request->religion;
        $user->marital_status = $request->marital_status;
        $user->profession = $request->profession;
        $user->phone_number = $request->phone_number;
        $user->is_active = 1;
        $user->save();

        $nik = $request->sin;
        $nik_unik = substr($nik, 0, 6);
        if ($nik_unik != '610107') {
            $user = User::find($user->id);
            $user->is_active = 2;
            $user->save();

            $messages = 'Mohon maaf akun anda tidak diaktifkan karena terdeteksi bukan merupakan warga parit baru';
            $data['user'] = $user;
            $data['messages'] = $messages;
            $email = [$user->email];
            // return view('email.user_approval',$data);

            try {
                Mail::send('email.user_approval', $data, function ($message) use ($email) {
                    $message->to($email)->subject('Akun Ditolak');
                });
            } catch (\Throwable $th) {
                //throw $th;
            }

            $arrError['message'] =  'Registrasi gagal, akun anda tidak diaktifkan karena terdeteksi bukan merupakan warga parit baru';
            $arrError['type'] = 'nik';
            $arrError['req'] = $request->except('photo');
            // dd($arrError);
            return redirect()->back()->with(
                'error',
                $arrError,
            );
        }

        // Validasi nik

        $messages = 'Selamat, anda berhasil melakukan registrasi';
        $data['user'] = $user;
        $data['messages'] = $messages;
        $email = [$user->email];
        // return view('email.user_approval',$data);

        Mail::send('email.user_approval', $data, function ($message) use ($email) {
            $message->to($email)->subject('Registrasi Berhasil');
        });

        Activity::add(['page' => 'Registrasi', 'description' => 'Melakukan Registrasi: ' . $request->name]);

        return redirect()->route('registrasi')->with(
            'success',
            'Selamat, anda berhasil melakukan registrasi',
        );
    }

    public function lupaPassword(Request $request)
    {
        $cek = User::where('email', $request->email)->first();
        if ($cek == null) {
            return redirect()->route('login')->with('gagal', 'Email Tidak Terdaftar');
        }

        $cek->forgot_password = md5($cek->id);
        $cek->save();

        $data['user'] = $cek;
        $email = [$cek->email];
        // return view('email.forgot_password',$data);
        Mail::send('email.forgot_password', $data, function ($message) use ($email) {
            $message->to($email)->subject('Reset Password');
        });

        return redirect()->route('login')->with('success', 'Silahkan cek email untuk mendapatkan akses reset password');
    }

    public function resetPassword($token)
    {
        $cek = User::where('forgot_password', $token)->first();
        if ($cek ==  null) {
            return redirect()->route('login')->with('gagal', 'Token tidak valid');
        }
        $data['user'] = $cek;
        return view('auth.lupa_password', $data);
    }

    public function lupaPasswordProses(Request $request, $id)
    {
        if ($request->password != $request->confirm_password) {
            return redirect()->back()->with('gagal', 'Password tidak sama !');
        } else {
            $user = User::findOrFail($id);
            $user->password = bcrypt($request->password);
            $user->forgot_password = null;
            $user->save();

            return redirect()->route('login')->with('success', 'Password berhasil di reset !');
        }
    }
}
