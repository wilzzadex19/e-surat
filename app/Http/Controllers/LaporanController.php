<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Letter;
use App\Letter_Archieve;
use App\Submission;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index($letter_id)
    {
        $submission = Submission::where('letter_id', $letter_id)->orderBy('number', 'asc')->get();
        $letter = Letter::find($letter_id);
        $data['letter'] = $letter;
        $data['submissions'] = $submission;
        return view('laporan.index', $data);
    }
    public function indexQ($letter_id)
    {
        $submission = Submission::where('letter_id', 4)->where('keterangan_pengajuan', $letter_id)->orderBy('number', 'asc')->get();
        $letter = Letter::find(4);
        $data['letter'] = $letter;
        $data['submissions'] = $submission;
        $data['k'] = $letter_id;
        return view('laporan.index', $data);
    }

    public function indexMasuk($jenis)
    {
        $arsip = Letter_Archieve::where('letter_category', $jenis)->where('is_surat_keluar_pemerintahan', null)->where('letter_id', null)->get();
        if ($jenis == 'keluar-pemerintahan') {
            $arsip = Letter_Archieve::where('is_surat_keluar_pemerintahan', 1)->where('letter_id', null)->get();
        }
        // dd($arsip);
        $data['arsip'] = $arsip;
        $data['label'] = ucfirst($jenis);
        $data['jenis'] = $jenis;


        if ($jenis == 'keluar-pemerintahan') {
            return view('laporan.keluar_pemerintahan', $data);
        }
        return view('laporan.masuk', $data);
    }

    public function cetak(Request $request)
    {
        // dd($request->all());
        if ($request->letter_id != 4) {
            $pisah = explode('/', $request->tanggal);
            $tanggal_awal = $pisah[0] . ' 00:00:00';
            $tanggal_akhir = $pisah[1] . ' 23:59:00';

            $submission = Submission::where('letter_id', $request->letter_id)->whereBetween('created_at', [$tanggal_awal, $tanggal_akhir])->orderBy('number', 'asc')->get();
            $letter = Letter::find($request->letter_id);
            $data['letter'] = $letter;
            $data['submissions'] = $submission;
            $data['judul'] = 'Laporan ' . $letter->name . ' <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
            // dd($data);



            return view('laporan.cetak', $data);
        } else {
            // dd($request->all());
            $pisah = explode('/', $request->tanggal);
            $tanggal_awal = $pisah[0] . ' 00:00:00';
            $tanggal_akhir = $pisah[1] . ' 23:59:00';

            $submission = Submission::where('letter_id', 4)->where('keterangan_pengajuan', $request->k)->whereBetween('created_at', [$tanggal_awal, $tanggal_akhir])->orderBy('number', 'asc')->get();
            $letter = Letter::find($request->letter_id);
            $data['letter'] = $letter;
            $data['submissions'] = $submission;
            $data['judul'] = 'Laporan Surat Keterangan (' . ($request->k == 1 ? '(Belum pernah melakukan perekaman E-KTP dan tidak memiliki E-KTP)' : '(Belum memiliki Akta Kelahiran)') . ') <br> Periode ' . date('d M Y', strtotime($tanggal_awal)) . ' s/d ' . date('d M Y', strtotime($tanggal_akhir));
            // dd($data);



            return view('laporan.cetak', $data);
        }
    }
}
