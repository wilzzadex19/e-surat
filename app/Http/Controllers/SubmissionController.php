<?php

namespace App\Http\Controllers;

use App\Submission;
use App\Letter;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Activity;
use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SubmissionController extends Controller
{
    public function index()
    {
        $submissions = Submission::where('user_id', auth()->id())->orderBy('id', 'desc')->get();

        return view('submissions', ['submissions' => $submissions]);
    }

    public function approved()
    {
        $submissions = Submission::with('user', 'letter', 'admin')
            ->where('approval_status', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('submissions.approved', ['submissions' => $submissions]);
    }
    public function selesai()
    {
        $submissions = Submission::with('user', 'letter', 'admin')
            ->where('approval_status', 3)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('submissions.selesai', ['submissions' => $submissions]);
    }

    public function store($letter, Request $request)
    {
        // dd($request->all());
        $letter = Letter::find($letter);
        // // dd($letter);

        // $awalan = $letter->number_format . '/';
        // $query = Submission::select('number')->where('letter_id', $letter->id)->orderBy('number', 'desc');

        // $lebar = 1;
        // $jumlahrecord = $query->count();

        // if ($jumlahrecord == 0) {
        //     $nomor = 1;
        // } else {
        //     $row = $query->get()->toArray();
        //     $nomor = intval(substr($row[0]['number'], strlen($awalan))) + 1;
        // }

        // if ($lebar > 0)
        //     $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        // else
        //     $angka = $awalan . $nomor;


        // $nomor_surat = $angka . '/Pem' . '/' . date('Y');





        if (!$letter) {
            return back()->with([
                'status' => 'danger',
                'message' => 'Surat Belum Tersedia!'
            ]);
        }

        $except = ['_token'];

        $submission = new Submission;
        $submission->user_id = auth()->id();
        $submission->number = '-';
        $submission->letter_id = $letter->id;
        $submission->kepentingan = $request->kepentingan_surat;

        if ($letter->id == 4) {
            $submission->keterangan_pengajuan = $request->keterangan_pengajuan;
        }

        foreach ($request->all() as $key => $valData) {
            if ($request->hasFile($key)) {
                $except[] .= $key;
                $file = $request->file($key);
                $path = 'submission_data/';


                if (!file_exists(public_path($path))) {
                    mkdir($path, 777, true);
                }

                $file_name = $key . '-' . time()  . '.' . $file->getClientOriginalExtension();
                $fullPath = $path  . $file_name;
                $file->move($path, $file_name);
                $request->request->add([$key . 's' => $fullPath]);
            }
        }
        $submission->data = json_encode($request->except($except));
        $submission->approval_status = 0;
        $submission->save();
        // 
        // $response = Http::withHeaders(['Authorization' => config('whatsapp.token')])
        // ->asForm()
        // ->post('https://fonnte.com/api/send_message.php', [
        //     'phone' => setting('whatsapp'),
        //     'type' => 'text',
        //     'text' => 'Ada Pengajuan Surat Baru Oleh: ' . auth()->user()->name . ' Dan Surat Yang Diajukan Adalah: ' . $letter->name . '. Mohon Segera Ditinjau, Terimakasih. E-Surat Pemerintah Desa fn s'
        // ]);

        // try {
            $admin_aktif = Admin::where('is_active', 1)->get();
            // dd($admin_aktif);

            foreach ($admin_aktif as $item) {
                $data['user'] = $item;
                $data['messages'] = Auth::user()->name . ' Telah mengajukan pengajuan ' . $letter->name . ' Pada ' . date('d M Y - H:i') . ', Mohon untuk segera di tindaklanjuti. <br> Terima Kasih!';
                // dd($data);
                $email = [$item->email];
                // return view('email.submission_notif',$data);

                Mail::send('email.submission_notif', $data, function ($message) use ($email) {
                    $message->to($email)->subject('Pengajuan Surat Oleh ' . Auth::user()->name );
                });
            }
        // } catch (\Throwable $th) {
        //     //throw $th;
        // }

        Activity::add(['page' => 'Pengajuan Surat', 'description' => 'Mengajukan Surat Baru: ' . $letter->name]);

        return back()->with([
            'status' => 'success',
            'message' => 'Berhasil Mengajukan Surat! Mohon tunggu notifikasi via Email'
        ]);
    }

    public function add()
    {

        $letters = Letter::all();
        $data['letters'] = $letters;
        return view('submission_add', $data);
    }
}
