<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Letter extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name', 'content', 'data', 'status','number_format'
    ];

    public function status()
    {
        return $this->status == 'On' ? 'Aktif' : 'Tidak Aktif';
    }

    public function submission(){
        return $this->hasMany(Submission::class,'letter_id','id');
    }

    public function getData()
    {
        return json_decode($this->data);
    }
}
