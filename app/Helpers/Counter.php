<?php

use App\Submission;
use App\User;

if (!function_exists('pendingSubmissions')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function pendingSubmissions()
    {
        return Submission::where('approval_status', 0)->count();
    }
}

if (!function_exists('pendingUsers')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function pendingUsers()
    {
        return User::where('is_active', 0)->count();
    }
}
