<?php

use App\Letter;
use App\Submission;
use App\User;
use Illuminate\Support\Facades\Mail;

if (!function_exists('lettersAll')) {
    function lettersAll()
    {
        return Letter::all();
    }
}

