<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Letter_Archieve extends Model
{
    use SoftDeletes;

    protected $table = 'letter_archieve';

    public function letter()
    {
        return $this->hasOne(Letter::class,'id','letter_id');
    }
    public function submission()
    {
        return $this->hasOne(Submission::class,'id','submission_id');
    }
}
